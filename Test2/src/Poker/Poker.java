package Poker;

public class Poker {
    public String suit;  //花色
    public int num; //数字

    public Poker(String suit, int num) {
        this.suit = suit;
        this.num = num;
    }

    @Override
    public String toString() {
//        return "poker{" +
//                "suit='" + suit + '\'' +
//                ", num=" + num +
//                '}';
        return suit+num;
    }
}
