package Poker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PokerGame {

    public static final String[] suits = {"♥", "♣", "♦", "♠"};

    public List<Poker> buyPoker() {
        List<Poker> pokerList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
//                String suit = suits[i];
//                Poker poker = new Poker(suit,j);
//                pokerList.add(poker);
                pokerList.add(new Poker(suits[i], j));  //简化写法
            }
        }
        return pokerList;
    }

    //洗牌
    public void shuffle(List<Poker> pokerList) {
        Random rand = new Random();
        for (int i = pokerList.size() - 1; i > 0; i--) {
            int index = rand.nextInt(i);
            swap(pokerList, i, index);
        }
    }

    private static void swap(List<Poker> pokerList, int i, int j) {
        Poker tmp = pokerList.get(i);
        pokerList.set(i,pokerList.get(j));
        pokerList.set(j,tmp);

    }

    public List<List<Poker>> getPoker(List<Poker> pokerList) {
        List<List<Poker>> hands = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            List<Poker> hand = new ArrayList<>();
            hands.add(hand);
        }

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                Poker poker = pokerList.remove(0);
                hands.get(j).add(poker);
            }
        }
        return hands;
    }

}
