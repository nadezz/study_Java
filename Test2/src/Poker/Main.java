package Poker;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PokerGame pokerGame = new PokerGame();
        List<Poker> ret = pokerGame.buyPoker();
        System.out.println("买牌:");
        System.out.println(ret);

        System.out.println("洗牌:");
        pokerGame.shuffle(ret);
        System.out.println(ret);

        System.out.println("揭牌:");
        List<List<Poker>> hands = pokerGame.getPoker(ret);
        for (int i = 0; i < 3; i++) {
            System.out.println("第" + (i + 1) + "个人的牌：" + hands.get(i));
        }

        System.out.println("剩下的牌：");
        System.out.println(ret);
    }
}
