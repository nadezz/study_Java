package Sort;

public class Sort3 {

    /**
     * 插入排序
     * @param arr
     */
    public static void insertSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int tmp = arr[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if(arr[j] > tmp) {
                    arr[j + 1] = arr[j];
                } else {
                    break;
                }
            }
            arr[j + 1] = tmp;
        }
    }

    /**
     * 希尔排序
     * @param arr
     */
    public static void shellSort(int[] arr) {
        int gap = arr.length;
        while (gap > 1) {
            gap /= 2;
            shell(arr,gap);
        }
    }

    public static void shell(int[] arr, int gap) {
        for (int i = gap; i < arr.length; i++) {
            int tmp = arr[i];
            int j = i - gap;
            for (; j >= 0; j -= gap) {
                if(arr[j] > tmp) {
                    arr[j + gap] = arr[j];
                } else {
                    break;
                }
            }
            arr[j + gap] = tmp;
        }
    }

    /**
     * 选择排序
     * @param arr
     */
    public static void selectSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }
            swap(arr,i,minIndex);
        }
    }

    /**
     * 优化版选择排序
     * @param arr
     */
    public static void selectSort2(int[] arr) {
        int left = 0;
        int right = arr.length - 1;
       while (left < right) {
           int minIndex = left;
           int maxIndex = left;
           for (int i = left + 1; i <= right; i++) {
               if(arr[minIndex] > arr[i]) {
                   minIndex = i;
               }
               if(arr[maxIndex] < arr[i]) {
                   maxIndex = i;
               }
           }
           //需要注意此情况
           if(maxIndex == left) {
               maxIndex = minIndex;
           }
           swap(arr,left++,minIndex);
           swap(arr,maxIndex,right--);
       }
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    /**
     * 冒泡排序
     * @param arr
     */
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            boolean flag = true;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if(arr[j] > arr[j+1]) {
                    swap(arr,j,j+1);
                    flag = false;
                }
            }
            if(flag) {
                break;
            }
        }
    }

    /**
     * 快速排序
     * @param arr
     */
    public static void quickSort(int[] arr) {
        quick(arr,0,arr.length - 1);
    }

    public static void quick(int[] arr, int left ,int right) {
        if(left < right) {
            swap(arr,left + (int)(Math.random() * (right - left + 1)),right);
            int[] ret = partition(arr,left,right);
            quick(arr,left,ret[0] - 1);
            quick(arr,ret[1] + 1,right);
        }
    }

    public static int[] partition(int[] arr, int left, int right) {
        int key = arr[right];
        int less = left - 1;
        int more = right;
        while (left < more) {
            if (arr[left] < key) {
                swap(arr,left,less+1);
                left++;
                less++;
            } else if (arr[left] > key) {
                swap(arr,left,more-1);
                more--;
            } else {
                left++;
            }
        }
        swap(arr,more,right);
        return new int[] {less+1,more};
    }

    /**
     * 归并排序
     * @param arr
     */
    public static void mergeSort(int[] arr) {
        mergeProcess(arr,0,arr.length - 1);
    }

    public static void mergeProcess(int[] arr, int left, int right) {
        if(left < right) {
            int mid = left + (right - left) / 2;
            mergeProcess(arr,left,mid);
            mergeProcess(arr,mid + 1,right);
            merge(arr,left,mid,right);
        }
    }

    public static void merge(int[] arr, int left, int mid, int right) {
        int[] helpArr = new int[right - left + 1];
        int p1 = left;
        int p2 = mid + 1;
        int i = 0;
        while (p1 <= mid && p2 <= right) {
            helpArr[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        while (p1 <= mid) {
            helpArr[i++] = arr[p1++];
        }
        while (p2 <= right) {
            helpArr[i++] = arr[p2++];
        }
        for (i = 0; i < helpArr.length; i++) {
            arr[left+i] = helpArr[i];
        }
    }

}
