package MyHeap;

import java.lang.annotation.ElementType;
import java.util.Arrays;

public class MyHeap3 {
    public int[] elem;
    public int usedSize;

    public MyHeap3() {
        elem = new int[10];
    }

    public void createHeap(int[] array) {
        for (int i = 0; i < array.length; i++) {
            elem[i] = array[i];
            usedSize++;
        }
        int parent = (usedSize - 1 - 1) / 2;
        for (int i = parent; i >= 0; i--) {
            shiftDown(i,usedSize);
        }
    }

    private void shiftDown(int parent,int len) {
        int child = parent * 2 + 1;
        while(child < len) {
            if(child + 1 < len && elem[child + 1] > elem[child]) {
                child++;
            }
            if(elem[child] > elem[parent]) {
                swap(child,parent);
                parent = child;
                child = parent * 2 + 1;
            } else {
                break;
            }
        }
    }

    public void swap(int a ,int b) {
        int tmp = elem[a];
        elem[a] = elem[b];
        elem[b] = tmp;
    }

    public void offer(int val) {
        if(isFull()) {
            elem = Arrays.copyOf(elem,elem.length * 2);
        }
        elem[usedSize] = val;
        usedSize++;
        shiftUp(usedSize-1);
    }

    private void shiftUp(int child) {
        int parent = (child - 1) / 2;
        while(elem[child] > elem[parent]) {
            swap(child,parent);
            child = parent;
            parent = (child - 1) / 2;
        }
    }

    public boolean isFull() {
        return usedSize == elem.length;
    }

    public int poll() {
        int ret = elem[0];
        swap(0,--usedSize);
        shiftDown(0,usedSize);
        return ret;
    }

    public boolean isEmpty() {
        return usedSize == 0;
    }

    public int peek() {
        return elem[0];
    }
}