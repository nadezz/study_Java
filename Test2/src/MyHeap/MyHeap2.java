package MyHeap;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MyHeap2 {
    public int[] elem;
    public int usedSize;

    public MyHeap2() {
        this.elem = new int[10];
    }

    public void initElem(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            elem[i] = arr[i];
            usedSize++;
        }
    }

    public void createBigHeap() {
        for(int parent = (usedSize - 1 - 1) / 2; parent >= 0; parent--) {
            siftDown(parent,usedSize);
        }
    }

    public void siftDown(int parent, int end) {
        int child = parent * 2 + 1;
        while(child < end) {
            if(child + 1 < end && elem[child+1] > elem[child]) {
                child++;
            }
            if(elem[child] > elem[parent]) {
                swap(elem,child,parent);
                parent = child;
                child = parent * 2 + 1;
            } else {
                break;
            }
        }
    }

    public int poll() {
        int ret = elem[0];
        swap(elem,0,usedSize-1);
        siftDown(0,--usedSize);
        return ret;
    }

    public void swap(int[] arr, int a, int b) {
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }

    public void offer(int val) {
        if(isFull()) {
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        elem[usedSize] = val;
        usedSize++;
        siftUp(usedSize-1);
    }

    private void siftUp(int child) {
        int parent = (child - 1) / 2;
        while(elem[child] > elem[parent]) {
            swap(elem,child,parent);
            child = parent;
            parent = (child - 1) / 2;
        }
    }

    public boolean isFull() {
        return usedSize == elem.length;
    }

}
