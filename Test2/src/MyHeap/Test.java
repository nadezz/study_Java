package MyHeap;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class Test {
    public static void main(String[] args) {
        int[] arr = { 27,15,19,18,28,34,65,49,25,37};
        MyHeap heap = new MyHeap();
        heap.initElem(arr);
        heap.createBigHeap();
        heap.offer(80);
        System.out.println("====");


    }

    /**
     * top-k问题  LeetCode面试题17
     * @param arr
     * @param k
     * @return
     */
    public int[] smallestK(int[] arr, int k) {
        int[] ret = new int[k];
        if(k == 0) {
            return ret;
        }
        Queue<Integer> queue = new PriorityQueue<>(new ComparatorBig());
        for (int i = 0; i < k; i++) {
            queue.offer(arr[i]);
        }
        for (int i = k; i < arr.length; i++) {
            if(queue.peek() > arr[i]) {
                queue.poll();
                queue.offer(arr[i]);
            }
        }
        for(int i = 0; i < k; i++) {
            ret[i] = queue.poll();
        }
        return ret;

    }

    static class ComparatorBig implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return o2-o1;
        }
    }
}
