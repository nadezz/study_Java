package MyHeap;


import java.util.Arrays;


public class MyHeap {

    public int[] elem;
    public int usedSize;

    public MyHeap() {
        this.elem = new int[10];
    }

    public void initElem(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            elem[i] = arr[i];
            usedSize++;
        }
    }

    //1、【整体调整】把整个数组堪称一棵树，从最后一棵子树开始，向下调整
    //2、【逐个插入调整】
    public void createBigHeap() {
        for (int parent = (usedSize - 1 - 1) / 2; parent >= 0; parent--) {
            siftDown(parent,usedSize);
        }
    }

    private void siftDown(int parent, int end) {
        int child = parent * 2 + 1;
        while(child < end) {
            if(child + 1 < end && elem[child + 1] > elem[child]) {
                child++;
            }
            if(elem[child] > elem[parent]) {
                swap(elem,child,parent);
                parent = child;
                child = parent * 2 + 1;
            } else {
                break;
            }
        }
    }

    public int poll() {
        int ret = elem[0];
        swap(elem,0,--usedSize);
        siftDown(0,usedSize);
        return ret;
    }

    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public void offer(int val) {
        if(isFull()) {
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        elem[usedSize] = val;
        usedSize++;
        siftUp(usedSize-1);
    }

    private void siftUp(int child) {
        int parent = (child - 1) / 2;
        while(elem[child] > elem[parent]) {
            swap(elem,child,parent);
            child = parent;
            parent = (child - 1) / 2;
        }
    }

    public boolean isFull() {
        return usedSize == elem.length;
    }

    /**
     * 堆排序【大根堆】--》从小到大排序
     */
    public void heapSort() {
        int end = usedSize - 1;
        while(end > 0) {
            swap(elem,0,end);
            siftDown(0,end-1);
            end--;
        }
    }
}
