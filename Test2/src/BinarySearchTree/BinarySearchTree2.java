package BinarySearchTree;

import com.sun.source.tree.Tree;

public class BinarySearchTree2 {

    static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode root;

    public boolean search(int val) {
        TreeNode cur = root;
        while (cur != null) {
            if(cur.val > val) {
                cur = cur.left;
            } else if (cur.val < val) {
                cur = cur.right;
            } else {
                return true;
            }
        }
        return false;
    }

    public void insert(int val) {
        if(root == null) {
            root = new TreeNode(val);
            return;
        }
        TreeNode cur = root;
        TreeNode prev = null;
        while(cur != null) {
            prev = cur;
            if(cur.val > val) {
                cur = cur.left;
            } else if (cur.val < val) {
                cur = cur.right;
            } else {
                return;   //相同退出
            }
        }
        if(prev.val > val) {
            prev.left = new TreeNode(val);
        } else {
            prev.right = new TreeNode(val);
        }
    }

    public void remove(int val) {
        TreeNode cur = root;
        TreeNode prev = null;
        while (cur != null) {
            prev = cur;
            if(cur.val > val) {
                cur = cur.left;
            } else if (cur.val < val) {
                cur = cur.right;
            } else {
                removeNode(cur,prev);
                return;
            }
        }
    }

    public void removeNode(TreeNode cur, TreeNode prev) {
        if (cur.left == null) {

            if(cur == root) {
                root = cur.right;
            } else if (prev.left == cur) {
                prev.left = cur.right;
            } else {
                prev.right = cur.right;
            }

        } else if (cur.right == null) {

            if(cur == root) {
                root = cur.left;
            } else if (prev.left == cur) {
                prev.left = cur.left;
            } else {
                prev.right = cur.left;
            }

        } else {

            TreeNode tmp = cur.right;
            TreeNode tmpPrev = cur;
            while (tmp.left != null) {
                tmpPrev = tmp;
                tmp = tmp.left;
            }
            cur.val = tmp.val;

            if(tmpPrev.left == tmp) {
                tmpPrev.left = tmp.right;
            } else {
                tmpPrev.right = tmp.right;
            }
        }
    }
}
