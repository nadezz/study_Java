package kaoshi;

import java.math.BigDecimal;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.DayOfWeek;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.*;

public class test {

    public boolean isSubsequence(String S, String T) {
        // write code here
        if (T.length() < S.length()) {
            return false;
        }
        Map<Character, Integer> map1 = new HashMap<>();
        Map<Character, Integer> map2 = new HashMap<>();
        for (int i = 0; i < T.length(); i++) {
            char tmp = T.charAt(i);
            if (!map1.containsKey(tmp)) {
                map1.put(tmp, 1);
            } else {
                int num = map1.get(tmp);
                map1.put(tmp, num + 1);
            }
        }
        for (int i = 0; i < S.length(); i++) {
            char tmp = S.charAt(i);
            if (!map2.containsKey(tmp)) {
                map2.put(tmp, 1);
            } else {
                int num = map2.get(tmp);
                map2.put(tmp, num + 1);
            }
        }
        for (int i = 0; i < S.length(); i++) {
            char tmp = S.charAt(i);
            if (!map1.containsKey(tmp) || map2.get(tmp).compareTo(map1.get(tmp)) > 0) {
                return false;
            }
        }
        return true;

    }

    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String str = in.nextLine();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            if (stack.isEmpty()) {
                stack.push(str.charAt(i));
            } else {
                if (stack.peek().equals(str.charAt(i))) {
                    stack.pop();
                } else {
                    stack.push(str.charAt(i));
                }
            }
        }
        ArrayList<Character> ret = new ArrayList<>();
        int sz = stack.size();
        for (int i = 0; i < sz; i++) {
            ret.add(stack.pop());
        }
        if (ret.isEmpty()) {
            System.out.println(0);
        } else {
            for (int i = ret.size() - 1; i >= 0; i--) {
                System.out.print(ret.get(i));
            }
        }
    }

    public static void main2(String[] args) {
        int[] nums = new int[8];
        System.out.println("=========");
    }

    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            if (n == 0) {
                break;
            }
            int empty = n;
            int count = 0;
            while (empty >= 2) {
                if (empty == 2) {
                    empty++;
                }
                int duihuan = empty / 3;
                count += duihuan;
                empty = empty % 3 + duihuan;
            }
            System.out.println(count);
        }
    }

    public static void main4(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.next();
        String str2 = sc.next();
        int maxIndex = 0;
        int maxCount = 0;
        for (int i = 0; i < str1.length(); i++) {
            int tmp = i;
            int count = 0;
            for (int j = 0; j < str2.length(); j++) {
                if (str1.charAt(tmp) == str2.charAt(j)) {
                    count++;
                    tmp++;
                } else {
                    break;
                }
            }
            if (maxCount < count) {
                maxIndex = i;
            }
            maxCount = Math.max(maxCount, count);
        }
        String ret = str1.substring(maxIndex, maxIndex + maxCount);
        System.out.println(ret);
    }

    public static void main5(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.next();
        for (int i = str.length() - 1; i >= 0; i--) {
            System.out.print(str.charAt(i));
        }
    }

    // 最长公共子串
    public static void main6(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.next();
        String str2 = sc.next();
        if (str1.length() < str2.length()) {
            System.out.println(getMaxSubstr(str1, str2));
        } else {
            System.out.println(getMaxSubstr(str2, str1));
        }
    }

    public static String getMaxSubstr(String str1, String str2) {
        int start = 0;
        int maxLen = 0;
        int len1 = str1.length();
        int len2 = str2.length();
        int[][] maxSubLen = new int[len1 + 1][len2 + 1];
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    maxSubLen[i][j] = maxSubLen[i - 1][j - 1] + 1;
                    if (maxSubLen[i][j] > maxLen) {
                        maxLen = maxSubLen[i][j];
                        start = i - maxLen;
                    }
                }
            }
        }
        return str1.substring(start, start + maxLen);
    }

    // 洗牌【24/3/6】
    public static void main7(String[] args) {
        Scanner sc = new Scanner(System.in);
        int group = sc.nextInt();
        while (group-- > 0) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            int[] nums = new int[2 * n];
            for (int i = 0; i < nums.length; i++) {
                nums[i] = sc.nextInt();
            }
            shuffle(nums, k);
        }
    }

    public static void shuffle(int[] nums, int k) {
        int[] helpArr = new int[nums.length];
        while (k-- > 0) {
            int left = nums.length / 2 - 1;
            int right = nums.length - 1;
            int i = helpArr.length - 1;
            for (; left >= 0 && right >= nums.length / 2; left--, right--) {
                helpArr[i--] = nums[right];
                helpArr[i--] = nums[left];
            }
            nums = Arrays.copyOf(helpArr, helpArr.length);
        }
        for (int i = 0; i < helpArr.length; i++) {
            if (i != helpArr.length - 1) {
                System.out.print(helpArr[i] + " ");
            } else {
                System.out.println(helpArr[i]);
            }
        }
    }

    // mp3 界面设置
    public static void main8(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        String commandStr = sc.next();
        char[] command = commandStr.toCharArray();
        if (count <= 4) {
            int current = 1;
            for (char c : command) {
                if (current == 1 && c == 'U') {
                    current = count;
                } else if (current == count && c == 'D') {
                    current = 1;
                } else if (c == 'U') {
                    current--;
                } else if (c == 'D') {
                    current++;
                }
            }
            for (int i = 1; i <= count; i++) {
                System.out.print(i + " ");
            }
            System.out.println();
            System.out.println(current);
        } else {
            mp3Interface(count, command);
        }
    }

    public static void mp3Interface(int count, char[] command) {
        int upLine = 1;
        int downLine = 4;
        int current = 1;
        for (char c : command) {
            if (c == 'U' && current == 1) {
                current = count;
                upLine = count - 3;
                downLine = count;
            } else if (c == 'D' && current == count) {
                current = 1;
                upLine = 1;
                downLine = 4;
            } else if (c == 'U' && upLine == current) {
                current--;
                upLine--;
                downLine--;
            } else if (c == 'D' && downLine == current) {
                current++;
                upLine++;
                downLine++;
            } else if (c == 'U') {
                current--;
            } else if (c == 'D') {
                current++;
            }
        }
        for (int i = upLine; i <= downLine; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println(current);
    }

    // 最终战力值【24/3/7】
    public static void main9(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            int a = sc.nextInt();
            int[] nums = new int[n];
            for (int i = 0; i < nums.length; i++) {
                nums[i] = sc.nextInt();
            }
            getFinalCombatCapability(nums, a);
        }
    }

    public static void getFinalCombatCapability(int[] nums, int a) {
        for (int x : nums) {
            if (a >= x) {
                a += x;
            } else {
                int ret = getGCD(x, a);
                a += ret;
            }
        }
        System.out.println(a);
    }

    public static int getGCD(int num1, int num2) {
        int tmp;
        while (num1 % num2 != 0) {
            tmp = num2;
            num2 = num1 % num2;
            num1 = tmp;
        }
        return num2;
    }

    // 字符串中不重复的第一个字符
    public static void main10(String[] args) {
        HashMap<Character, Integer> hashMap = new HashMap<>();
        Scanner sc = new Scanner(System.in);
        char[] arr = sc.next().toCharArray();
        for (int i = 0; i < arr.length; i++) {
            hashMap.put(arr[i], 0);
        }
        for (int i = 0; i < arr.length; i++) {
            hashMap.put(arr[i], hashMap.get(arr[i]) + 1);
        }
        for (int i = 0; i < arr.length; i++) {
            if (hashMap.get(arr[i]) == 1) {
                System.out.println(arr[i]);
                return;
            }
        }
        System.out.println(-1);
    }

    public int getValue(int[] gifts, int n) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int gift : gifts) {
            hashMap.put(gift, 0);
        }
        for (int gift : gifts) {
            hashMap.put(gift, hashMap.get(gift) + 1);
        }
        for (int gift : gifts) {
            if (hashMap.get(gift) > (n / 2.0)) {
                return gift;
            }
        }
        return 0;
    }

    public static void main11(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            char[] arr1 = sc.next().toCharArray();
            char[] arr2 = sc.next().toCharArray();
            int len1 = arr1.length;
            int len2 = arr2.length;
            int count = 0;
            HashSet<Character> hashSet = new HashSet<>();
            for (char c : len1 > len2 ? arr2 : arr1) {
                hashSet.add(c);
            }
            for (char c : len1 > len2 ? arr1 : arr2) {
                if (hashSet.add(c)) {
                    count++;
                }
            }
            System.out.println(count);
        }

    }

    // =============
    public int getMost(int[][] board) {
        // write code here
        int count = 0;
        return getMostChild(board, 0, 0, count);
    }

    private int getMostChild(int[][] board, int row, int col, int count) {
        count += board[row][col];
        int downMost = 0;
        int rightMost = 0;
        if (col + 1 <= board[0].length) {
            downMost = getMostChild(board, row, col + 1, count);
        } else if (row + 1 <= board.length) {
            downMost = getMostChild(board, row + 1, col, count);
        }
        if (row + 1 <= board.length) {
            rightMost = getMostChild(board, row + 1, col, count);
        } else if (col + 1 <= board[0].length) {
            rightMost = getMostChild(board, row, col + 1, count);
        }
        return count;
    }

    // 两个字符串中的最长公共子串
    // https://www.nowcoder.com/practice/181a1a71c7574266ad07f9739f791506?tpId=37&&tqId=21288&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking
    public static void main12(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.next();
        String str2 = sc.next();

        if (str1.length() > str2.length()) {
            getMaxSubString(str2, str1);
        } else {
            getMaxSubString(str1, str2);
        }

    }

    public static void getMaxSubString(String str1, String str2) {
        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();
        int[][] result = new int[arr1.length + 1][arr2.length + 1];
        int maxLen = 0;
        int maxIndex = 0;
        for (int i = 1; i <= arr1.length; i++) {
            for (int j = 1; j <= arr2.length; j++) {
                if (arr1[i - 1] == arr2[j - 1]) {
                    result[i][j] = result[i - 1][j - 1] + 1;
                }
                if (maxLen < result[i][j]) {
                    maxLen = result[i][j];
                    maxIndex = i - maxLen;
                }
            }
        }
        System.out.println(str1.substring(maxIndex, maxIndex + maxLen));
    }

    public static void main13(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String line = sc.nextLine();
            while (line.length() > 1) {
                int sum = 0;
                for (int i = 0; i < line.length(); i++) {
                    sum += line.charAt(i) - '0';
                }
                line = String.valueOf(sum);
            }
            System.out.println(line);
        }
    }

    // 年终奖问题
    // https://www.nowcoder.com/practice/72a99e28381a407991f2c96d8cb238ab?tpId=134&tqId=33854&ru=/exam/oj
    public int getMostBonus(int[][] board) {
        // write code here
        int row = board.length;
        int col = board[0].length;
        for (int i = 1; i < row; i++) {
            board[i][0] += board[i - 1][0];
        }
        for (int j = 1; j < col; j++) {
            board[0][j] += board[0][j - 1];
        }
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                board[i][j] += Math.max(board[i - 1][j], board[i][j - 1]);
            }
        }
        return board[row - 1][col - 1];
    }

    // 迷宫问题
    // https://www.nowcoder.com/practice/cf24906056f4488c9ddb132f317e03bc?tpId=37&tqId=21266&ru=/exam/oj
    static class Node {
        int x;
        int y;

        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main14(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] mat = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                mat[i][j] = sc.nextInt();
            }
        }
        ArrayList<Node> path = new ArrayList<>();
        ArrayList<Node> minPath = new ArrayList<>();
        int[][] book = new int[row][col];
        getMinPath(mat, row, col, 0, 0, book, path, minPath);
        for (Node node : minPath) {
            System.out.println("(" + node.x + "," + node.y + ")");
        }
    }

    public static void getMinPath(int[][] mat, int row, int col,
                                  int x, int y, int[][] book, ArrayList<Node> path, ArrayList<Node> minPath) {
        // (x,y) 越界、走过或者有障碍时
        if (x < 0 || x >= row || y < 0 || y >= col
                || book[x][y] == 1 || mat[x][y] == 1) {
            return;
        }
        // 保存路径，标记为已走过
        path.add(new Node(x, y));
        book[x][y] = 1;
        // 判断是否为出口
        if (x == row - 1 && y == col - 1) {
            // 判断是否是最短路径
            if (minPath.isEmpty() || minPath.size() > path.size()) {
                minPath.clear();
                minPath.addAll(path);
            }
        }
        // 继续搜索上下左右四个方向
        getMinPath(mat, row, col, x - 1, y, book, path, minPath);
        getMinPath(mat, row, col, x + 1, y, book, path, minPath);
        getMinPath(mat, row, col, x, y - 1, book, path, minPath);
        getMinPath(mat, row, col, x, y + 1, book, path, minPath);
        // 越界、走过或者有障碍返回时
        path.remove(path.size() - 1);
        book[x][y] = 0;
    }

    // 快到碗里来
    public static void main15(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {

            BigDecimal catLen = sc.nextBigDecimal();
            BigDecimal bowlR = sc.nextBigDecimal();
            BigDecimal mul = new BigDecimal("3.14").multiply(new BigDecimal("2"));
            BigDecimal len = bowlR.multiply(mul);
            System.out.println(catLen.compareTo(len) > 0 ? "Yes" : "No");
        }
    }

    // 三角形
    public static void main16(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            BigDecimal a = sc.nextBigDecimal();
            BigDecimal b = sc.nextBigDecimal();
            BigDecimal c = sc.nextBigDecimal();

            if (a.compareTo(b) > 0) {
                BigDecimal tmp = a;
                a = b;
                b = tmp;
            }
            if (a.compareTo(c) > 0) {
                BigDecimal tmp = a;
                a = c;
                c = tmp;
            }
            if (b.compareTo(c) > 0) {
                BigDecimal tmp = b;
                b = c;
                c = tmp;
            }

            if (a.add(b).compareTo(c) > 0 && c.subtract(b).compareTo(a) < 0) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }

    public static void main17(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int[] nums = new int[number + 1];
        for (int i = 1; i < nums.length; i++) {
            nums[i] = sc.nextInt();
        }
        int left = sc.nextInt();
        int right = sc.nextInt();
        while (left < right) {
            swap(nums, left, right);
            left++;
            right--;
        }
        for (int i = 1; i < nums.length; i++) {
            System.out.print(nums[i] + " ");
        }
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    // 有假币
    // https://www.nowcoder.com/questionTerminal/1d18c0841e64454cbc3afaea05e2f63c
    public static void main18(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            if (n == 0) {
                break;
            }
            int count = 0;
            while (n >= 2) {
                n = (int) Math.ceil((double) n / 3);
                count++;
            }
            System.out.println(count);
        }
    }


    // 求正数数组的最小不可组成和
    // https://www.nowcoder.com/questionTerminal/296c2c18037843a7b719cf4c9c0144e4
    public static int getFirstUnFormedNum(int[] arr) {
        int min = Integer.MAX_VALUE;
        int max = 0;
        for (int i : arr) {
            min = Math.min(min, i);
            max += i;
        }
        boolean[] result = new boolean[max + 1];
        result[0] = true;
        for (int i = 0; i < arr.length; i++) {
            for (int j = max; j >= arr[i]; j--) {
                if (result[j - arr[i]]) {
                    result[j] = true;
                }
            }
        }
        for (int i = min; i <= max; i++) {
            if (!result[i]) {
                return i;
            }
        }
        return max + 1;
    }

    public static void main19(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String str = sc.nextLine();
            System.out.println(decode(str));
        }
    }

    private static String decode(String str) {
        char[] arr = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : arr) {
            if (c != ' ') {
                c -= 5;
                if (c < 'A') {
                    c += 26;
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }

    // 因子个数  【超时】   原因在于 i <= num
    public static void main20(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int num = sc.nextInt();
            //int tmp = num;
            int count = 0;
            for (int i = 2; i <= num; i++) {
                boolean flag = false;
                while (num % i == 0) {
                    if (!flag) {
                        count++;
                        flag = true;
                    }
                    num /= i;
                }
            }
            System.out.println(count);
        }
    }

    // 因子个数
    // https://ac.nowcoder.com/acm/problem/267775
    public static void main21(String[] args) {
        Scanner sc = new Scanner(System.in);
        long num = sc.nextLong();
        int count = 0;
        for (int i = 2; i < Math.sqrt(num); i++) {
            if (num % i == 0) {
                while (num % i == 0) {
                    num /= i;
                }
                count++;
            }
        }
        if (num != 1) {
            count++;
        }
        System.out.println(count);
    }

    // 3/25 未完成
    public static void main22(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int year = sc.nextInt();

        }
    }

    public static void getFestival(int year, int month, int serialNum, int whatDay) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(date);
        System.out.println(format);
    }

    // 分解因数吧
    public static void main23(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            List<Integer> list = new ArrayList<>();
            int num = sc.nextInt();
            if (isPrimeNum(num)) {
                System.out.println(num + " = " + num);
                break;
            }
            int tmp = num;
            int divisor = 2;
            while (num / divisor != 1) {
                if (num % divisor == 0) {
                    num /= divisor;
                    list.add(divisor);
                } else {
                    divisor = getNext(divisor);
                }
            }
            list.add(num);
            System.out.print(tmp + " = ");
            for (int i = 0; i < list.size(); i++) {
                if (i < list.size() - 1) {
                    System.out.print(list.get(i) + " * ");
                } else {
                    System.out.println(list.get(i));
                }
            }
        }
    }

    public static int getNext(int cur) {
        int next = cur + 1;
        while (!isPrimeNum(next)) {
            next++;
        }
        return next;
    }

    private static boolean isPrimeNum(int next) {
        for (int i = 2; i < Math.sqrt(next); i++) {   //  <=??   这里的问题吗??
            if (next % i == 0) {
                return false;
            }
        }
        return true;
    }


    public static void main111(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] month = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        while (sc.hasNext()) {
            int yearStart = sc.nextInt();
            int monthStart = sc.nextInt();
            int dayStart = sc.nextInt();
            int yearEnd = sc.nextInt();
            int monthEnd = sc.nextInt();
            int dayEnd = sc.nextInt();
            int count = 0;
            for (int i = yearStart; i <= yearEnd; i++) {
                if (i != yearEnd && i != yearStart) {
                    if (is_r(i)) {
                        count += 580;
                    } else {
                        count += 579;
                    }
                } else if (i == yearStart) {
                    if (monthStart <= 2 && is_r(i)) {

                    } else {

                    }
                } else {

                }

            }
        }
    }

    private static boolean is_r(int i) {
        return true;
    }

    public static void main24(String[] args) {
        Scanner sc = new Scanner(System.in);
        int mod = (int) 10e5;
        // 注意 hasNext 和 hasNextLine 的区别
        while (sc.hasNextInt()) { // 注意 while 处理多个 case
            int num = sc.nextInt();
            int count = 2;
            int a = 0;
            int b = 1;
            int c = 1;
            while (count <= num) {
                a = b;
                b = c;
                c = (a + b) % mod;
                count++;
            }
            System.out.println(c);
        }
    }

    // 剪花布条
    public static void main25(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String str1 = sc.next();
            String str2 = sc.next();
            int i = 0;
            int j = 0;
            int count = 0;
            while (i < str1.length()) {
                if (str1.charAt(i) == str2.charAt(j)) {
                    if (j == str2.length() - 1) {
                        count++;
                        j = 0;
                        i++;
                    } else {
                        i++;
                        j++;
                    }
                } else {
                    if (j == 0) {
                        i++;
                    } else {
                        j = 0;
                    }
                }
            }
            System.out.println(count);
        }
    }


    // 客似云来
    // https://www.nowcoder.com/questionTerminal/3549ff22ae2c4da4890e9ad0ccb6150d
    public static void main26(String[] args) {
        Scanner sc = new Scanner(System.in);
        long[] nums = new long[81];
        nums[0] = 0;
        nums[1] = 1;
        nums[2] = 1;
        for (int i = 3; i < nums.length; i++) {
            nums[i] = nums[i - 1] + nums[i - 2];
        }
        while (sc.hasNextInt()) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            long count = 0;
            for (int i = from; i <= to; i++) {
                count += nums[i];
            }
            System.out.println(count);
        }
    }

    // 收件人列表
    public static void main27(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int n = sc.nextInt();
            sc.nextLine();
            while (n-- > 0) {
                String str = sc.nextLine();
                if (exist(str)) {
                    System.out.print("\"");
                    System.out.print(str);
                    System.out.print("\"");
                } else {
                    System.out.print(str);
                }
                if (n != 0) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }

    }

    private static boolean exist(String str) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == ',' || c == ' ') {
                return true;
            }
        }
        return false;
    }

    // 养兔子
    public static void main28(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            long num = sc.nextInt();
            long a = 0;
            long b = 1;
            long c = 0;
            for (int i = 0; i < num; i++) {
                c = a + b;
                a = b;
                b = c;
            }
            System.out.println(c);
        }
    }

    // 年会抽奖
    public static void main29(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()) {
            int num = sc.nextInt();
            double resultPro = 1.0;
            double curPro = (num - 1.0) / num;
            int tmp = num;
            while (tmp-- > 1) {
                resultPro *= curPro;
                curPro = (tmp - 1.0) / tmp;
            }
            System.out.printf("%5.2f", resultPro * 100);
            System.out.println("%");
        }
    }

    // 抄送列表
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String str1 = sc.nextLine();
            String str2 = sc.nextLine();
            int i = 0;
            int j = 0;
            boolean flag = false;
            int start = -1;
            while (i < str1.length()) {
                if (str1.charAt(i) != str2.charAt(j)) {
                    i = i - j + 1;
                    j = 0;
                } else {
                    if (j == str2.length() - 1) {
                        start = i - j;
                        flag = true;
                        break;
                    }
                    i++;
                    j++;
                }
            }
            if (flag) {
                if (start == 0 && (i == str1.length() - 1 || str1.charAt(i + 1) == ',')) {
                    System.out.println("Ignore");
                } else if (start != 0 && i + 1 != str1.length()) {
                    if (str1.charAt(start - 1) == ',' && str1.charAt(i + 1) == ',') {
                        System.out.println("Ignore");
                    } else if (str1.charAt(start - 1) == '"' && str1.charAt(i + 1) == '"') {
                        System.out.println("Ignore");
                    } else {
                        System.out.println("Important!");
                    }
                } else {
                    System.out.println("Important!");
                }
            } else {
                System.out.println("“Important!");
            }
        }
    }
}
