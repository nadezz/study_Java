package Exception;

public class UserPasswordException extends RuntimeException {
    public UserPasswordException() {
        super();
    }

    public UserPasswordException(String s) {
        super(s);
    }
}
