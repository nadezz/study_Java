package Exception;

class Login {
    public String userName = "admin";
    public String passWord = "123456";

    public void login(String userName, String passWord) {
        if(!this.userName.equals(userName)) {
            //System.out.println("用户名错误！");
            throw new UserNameException("用户名错误！");
        }

        if(!this.passWord.equals(passWord)) {
            //System.out.println("密码错误！");
            throw new UserPasswordException("密码错误！");
        }
    }
}

public class Main {

    public static void main(String[] args) {
        try {
            Login l = new Login();
            l.login("admin", "123");
        } catch (UserNameException | UserPasswordException e) {
            e.printStackTrace();
        }

    }
}
