package MyQueue;

public class MyQueue2 {
    static class ListNode {
        public int val;
        public ListNode next;
        private ListNode prev;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode head;
    public ListNode last;

    public void offer(int val) {
        ListNode node = new ListNode(val);
        if(head == null) {
            head = last = node;
        } else {
            last.next = node;
            node.prev = last;
            last = node;
        }
    }

    public void poll() {
        if(head == null) {
            return;
        }
        if(head.next == null) {
            head = last = null;
        } else {
            head = head.next;
            head.prev = null;
        }
    }

    public int peek() {
        if(head == null) {
            return -1;
        }
        return head.val;
    }

}
