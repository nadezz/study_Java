package MySingleList;

import java.util.List;

public class MySingleList implements IList{
    static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode head;

    public void createList() {
        ListNode node1 = new ListNode(12);
        ListNode node2 = new ListNode(23);
        ListNode node3 = new ListNode(34);
        ListNode node4 = new ListNode(45);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        this.head = node1;
    }


    @Override
    public void addFirst(int data) {
        ListNode node = new ListNode(data);
        node.next = this.head;
        this.head = node;
    }

    @Override
    public void addLast(int data) {
        ListNode node = new ListNode(data);
        if(head == null) {
            head = node;
        } else {
            ListNode cur = head;
            while(cur.next != null) {
                cur = cur.next;
            }
            cur.next = node;
        }

    }

    @Override
    public void addIndex(int index, int data) throws IndexException{
        if(index < 0 || index > size()) {
            throw new IndexException("index不合法的：" + index);
        }
        ListNode node = new ListNode(data);
        if(head == null) {
            head = node;
            return;
        }
        //头插
        if(index == 0) {
            addFirst(data);
            return;
        }
        //尾插
        if(index == size()) {
            addLast(data);
            return;
        }
        //中间插入
        ListNode cur = searchPrevIndex(index);
        node.next = cur.next;
        cur.next = node;

    }

    private ListNode searchPrevIndex(int index) {
        ListNode cur = this.head;
        for (int i = 0; i < index - 1; i++) {
            cur = cur.next;
        }
        return cur;
    }

    @Override
    public boolean contains(int key) {
        ListNode cur = this.head;
        while(cur != null) {
            if(cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    @Override
    public void remove(int key) {
        if(head == null) {
            return;
        }
        if(head.val == key) {
            head = head.next;
            return;
        }
        ListNode prev = findPrevKey(key);
        if(prev == null) {
            return; //没有要删除的数字
        }
        prev.next = prev.next.next;

    }

    private ListNode findPrevKey(int key) {
        ListNode prev = this.head;
        while(prev.next != null) {
            if(prev.next.val == key) {
                return prev;
            } else {
                prev = prev.next;
            }
        }
        return null;
    }

    @Override
    public void removeAllKey(int key) {
        if(head == null) {
            return;
        }
        //写在前面用while
//        while(head.val == key) {
//            head = head.next;
//        }
        ListNode cur = head.next;
        ListNode prev = head;
        while (cur != null) {
            if(cur.val == key) {
                prev.next = cur.next;
                cur = cur.next;
            } else {
                prev = cur;
                cur = cur.next;
            }
        }
        //剩下头结点没有判断
        //写在后面用if
        if(head.val == key) {
            head = head.next;
        }
    }

    @Override
    public int size() {
        int count = 0;
        ListNode cur = this.head;
        while(cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public void display() {
        ListNode cur = head;
        while(cur != null) {
            System.out.print(cur.val+" ");
            cur = cur.next;
        }
        System.out.println();
    }



}
