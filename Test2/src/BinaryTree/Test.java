package BinaryTree;

public class Test {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        BinaryTree2 binaryTree2 = new BinaryTree2();
        BinaryTree.TreeNode root = binaryTree.createTree();
        BinaryTree2.TreeNode root2 = binaryTree2.createTree();
        System.out.print("先序：");
        binaryTree.preOrder(root);
        System.out.println();

        System.out.print("中序：");
        binaryTree.inOrder(root);
        System.out.println();

        System.out.print("后序：");
        binaryTree.postOrder(root);
        System.out.println();
        System.out.println("节点个数：");
        System.out.println(binaryTree.size(root));
        System.out.println(binaryTree2.size(root2));
        System.out.println("叶子节点个数：");
        System.out.println(binaryTree.getLeafNodeCount(root));
        System.out.println(binaryTree2.getLeafNodeCount(root2));
        System.out.println("第3层有多少个节点：");
        System.out.println(binaryTree.getKLevelNodeCount(root,3));
        System.out.println(binaryTree2.getKLevelNodeCount(root2,3));
        System.out.println("树的高度:");
        System.out.println(binaryTree.getHeight(root));
        System.out.println(binaryTree2.getHeight(root2));
        System.out.println("寻找值为E的节点");
        System.out.println(binaryTree.find(root,'E').val);
        System.out.println(binaryTree2.find(root2,'E').val);
        System.out.println(binaryTree.find(root,'F').val);
        System.out.println(binaryTree2.find(root2,'F').val);

        binaryTree2.widthOrder(root2);
        System.out.println();

        System.out.println("判断完全二叉树:"+binaryTree.isCompleteTree(root));


    }
}
