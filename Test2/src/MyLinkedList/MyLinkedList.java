package MyLinkedList;

import MyArrayList.EmptyException;
import MySingleList.IList;
import MySingleList.IndexException;

import java.util.List;

public class MyLinkedList implements IList {

    static class ListNode {
        public int val;
        public ListNode next;
        public ListNode prev;

        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode head;
    public ListNode last;

    @Override
    public void addFirst(int data) {
        ListNode node = new ListNode(data);
        if(head == null) {
            head = last = node;
        } else {
            node.next = head;
            head.prev = node;
            head = node;
        }
    }

    @Override
    public void addLast(int data) {
        ListNode node = new ListNode(data);
        if(last == null) {
            head = last = node;
        } else {
            last.next = node;
            node.prev = last;
            last = node;
        }
    }

    @Override
    public void addIndex(int index, int data) {
        if(index < 0 || index > size()) {
            throw new IndexException("双向链表插入，index不合法：" + index);
        }
        if(index == 0) {
            addFirst(data);
        } else if (index == size()) {
            addLast(data);
        } else {
            ListNode node = new ListNode(data);
            ListNode cur = findNodeByIndex(index);
            node.next = cur;
            cur.prev.next = node;
            node.prev = cur.prev;
            cur.prev = node;
        }
    }

    private ListNode findNodeByIndex(int index) {
        ListNode cur = head;
        while(index-- > 0) {
            cur = cur.next;
        }
        return cur;
    }

    @Override
    public boolean contains(int key) {
        ListNode cur = head;
        while(cur != null) {
            if(cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    @Override
    public void remove(int key) {
        ListNode cur = findNodeByKey(key);
        if(cur == head) {  //删头
            if(head.next == null) {  //只有一个节点时
                head = null;
                last = null;
                return;
            }
            head = head.next;
            head.prev = null;
        } else if(cur == last) {  //删尾
            last = last.prev;
            last.next = null;
        } else if(cur != null){   //删中间
            cur.prev.next = cur.next;
            cur.next.prev = cur.prev;
        }
    }

    private ListNode findNodeByKey(int key) {
        ListNode cur = head;
        while(cur != null) {
            if(cur.val == key) {
                return cur;
            }
            cur = cur.next;
        }
        return null;
    }

    @Override
    public void removeAllKey(int key) {
        ListNode cur = head;
        while(cur != null) {
            if(cur.val == key) {
                if(cur == head) {  //删头
                    if(head.next == null) {  //只有一个节点时
                        head = null;
                        return;
                    }
                    head = head.next;
                    head.prev = null;
                } else if(cur == last) {  //删尾
                    last = last.prev;
                    last.next = null;
                } else {   //删中间
                    cur.prev.next = cur.next;
                    cur.next.prev = cur.prev;
                }
            }

            cur = cur.next;
        }
    }

    @Override
    public int size() {
        int count = 0;
        ListNode cur = head;
        while(cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }

    @Override
    public void clear() {
        ListNode cur = head;
        while (cur != null) {
            ListNode curNext = cur.next;
            cur.next = null;
            cur.prev = null;
            cur = curNext;
        }
        //也可以只写下面这一部分直接置为空
        this.head = null;
        this.last = null;
    }

    @Override
    public void display() {
        ListNode cur = head;
        while(cur != null) {
            System.out.print(cur.val+" ");
            cur = cur.next;
        }
        System.out.println();
    }
}
