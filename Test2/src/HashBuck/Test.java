package HashBuck;

import java.util.*;

public class Test {

    static class Student {
        public String id;

        public Student(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "student{" +
                    "id='" + id + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student student = (Student) o;
            return Objects.equals(id, student.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }

    public static void main(String[] args) {
        Student student1 = new Student("123");
        Student student2 = new Student("123");
        int hashcode1 = student1.hashCode();
        int hashcode2 = student2.hashCode();
        System.out.println(hashcode1);
        System.out.println(hashcode2);
    }


}
