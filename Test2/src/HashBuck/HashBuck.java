package HashBuck;

public class HashBuck {
    static class Node {
        public int key;
        public int val;
        public Node next;

        public Node(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    public Node[] array = new Node[10];
    public int usedSize;
    public static final double LOAD_FACTOR = 0.75;   //负载因子边界

    public void push(int key, int val) {
        Node node = new Node(key, val);
        //1. 找到位置
        int index = key % array.length;
        //2. 遍历数组下标的链表
        Node cur = array[index];
        /*if (cur == null) {   //链表为空   //此处不需要也可以，因为下面代码包括了这种情况
            array[index] = node;
            usedSize++;
            return;
        }*/
        while (cur != null) {
            if (cur.key == key) {
                cur.val = val;   //key存在则更新，不存在则添加
                return;
            }
            cur = cur.next;
        }
        node.next = array[index];
        array[index] = node;
        usedSize++;
        //计算负载因子，并考虑是否重新哈希
        if (doLoadFactor() >= LOAD_FACTOR) {
            //重新哈希
            reSize();
        }
    }

    private void reSize() {
        Node[] newArray = new Node[array.length*2];
        //处理重新哈希
        for (int i = 0; i < array.length; i++) {
            Node cur = array[i];
            while (cur != null) {
                Node curNext = cur.next;

                int index = cur.key % newArray.length;
                cur.next = newArray[index];
                newArray[index] = cur;
                cur = curNext;
            }
        }

        array = newArray;
    }

    private double doLoadFactor() {
        return usedSize * 1.0 / array.length;
    }

    public int get(int key) {
        int index = key % array.length;
        Node cur = array[index];
        while (cur != null) {
            if (cur.key == key) {
                return cur.val;
            }
            cur = cur.next;
        }
        return -1;
    }
}
