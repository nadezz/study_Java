package HashBuck;

public class HashBuckGenerics<K,V> {

    static class Node<K,V> {
        public K key;
        public V val;
        public Node<K,V> next;

        public Node(K key, V val) {
            this.key = key;
            this.val = val;
        }
    }

    public Node<K,V>[] array = new Node[10];
    public int usedSize;
    public static final double LOAD_FACTOR = 0.75;

    public void push(K key, V val) {
        Node<K,V> node = new Node<>(key,val);

        int index = key.hashCode() % array.length;
        Node<K,V> cur = array[index];
        while (cur != null) {
            if (cur.key.equals(key)) {
                cur.val = val;
                return;
            }
            cur = cur.next;
        }

        node.next = array[index];
        array[index] = node;
        usedSize++;
        //重新哈希
    }

    public V get(K key) {
        int index = key.hashCode() % array.length;
        Node<K,V> cur = array[index];
        while (cur != null) {
            if(cur.key.equals(key)) {
                return cur.val;
            }
            cur = cur.next;
        }
        return null;
    }
}
