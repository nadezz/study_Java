package MyArrayList;

import java.util.Arrays;

public class MyArrayList implements IList{

    public int[] elem;
    public int usedSize;

    public static final int DEFAULT_CAPACITY = 5;
    public MyArrayList() {
        elem = new int[DEFAULT_CAPACITY];
    }

    @Override
    public void display() {
        for (int i = 0; i < usedSize; i++) {
            System.out.println(elem[i]+ " ");
        }
        System.out.println();
    }
    @Override
    public void add(int data) {
        if(isFull()) {
            //二倍扩容
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        elem[usedSize++] = data;
    }

    @Override
    public void add(int pos, int data) {
        checkPosOfAdd(pos);
        if(isFull()) {
            //二倍扩容
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        for(int i = usedSize - 1; i >= pos; i--) {
            elem[i + 1] = elem[i];
        }
        elem[pos] = data;
        usedSize++;
    }

    private void checkPosOfAdd(int pos) {
        if(pos < 0 || pos > usedSize) {
            throw new PosException("pos位置为：" + pos);
        }
    }

    @Override
    public boolean isFull() {
        return usedSize == elem.length;
    }

    /**
     * 查找当前元素 是否存在
     * @param toFind
     * @return
     */
    @Override
    public boolean contains(int toFind) {
        for (int i = 0; i < usedSize; i++) {
            if(toFind == elem[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找当前元素 的下标
     * @param toFind
     * @return
     */
    @Override
    public int indexOf(int toFind) {
        for (int i = 0; i < usedSize; i++) {
            if(toFind == elem[i]) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int get(int pos) {
        checkPosOfGet(pos);
        if(isEmpty()) {
            throw new EmptyException("顺序表为空");
        }
        return this.elem[pos];
    }

    @Override
    public boolean isEmpty() {
        return usedSize == 0;
    }

    private void checkPosOfGet(int pos) {
        if(pos < 0 || pos >= this.usedSize) {
            throw new PosException("pos位置不合法："+pos);
        }
    }

    @Override
    public void set(int pos, int value) {
        checkPosOfGet(pos);
        if(isEmpty()) {
            throw new EmptyException("顺序表为空");
        }
        this.elem[pos] = value;
    }

    @Override
    public void remove(int toRemove) {
        if(isEmpty()) {
            throw new EmptyException("顺序表为空，不能删除");
        }
        int index = indexOf(toRemove);
        for(int i = index; i < usedSize - 1; i++) {
            elem[i] = elem[i + 1];
        }
        this.usedSize--;

    }

    @Override
    public int size() {
        return this.usedSize;
    }

    @Override
    public void clear() {
        this.usedSize = 0;
    }


}
