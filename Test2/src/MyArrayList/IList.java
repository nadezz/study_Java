package MyArrayList;

public interface IList {
    void add(int data);
    void add(int pos, int data);
    boolean contains(int toFind);
    int indexOf(int toFind);
    int get(int pos);
    void set(int pos, int value);
    void remove(int toRemove);
    int size();
    void clear();
    void display();
    boolean isFull();
    boolean isEmpty();
}
