package MyStack;

import MyArrayList.EmptyException;

import java.util.Arrays;

public class MyStack {
    private int[] elem;
    private int usedSize;
    public static final int DEFAULT_CAPACITY = 10;
    public MyStack() {
        this.elem = new int[DEFAULT_CAPACITY];
    }

    public void push(int val) {
        if(isFull()) {
            this.elem = Arrays.copyOf(elem,2 * elem.length);
        }
        elem[usedSize++] = val;
    }

    public boolean isFull() {
        return usedSize == elem.length;
    }
    public int pop() {
        if(isEmpty()) {
            throw new EmptyException("栈为空...");
        }
        usedSize--;
        return elem[usedSize];
    }

    public boolean isEmpty() {
        return usedSize == 0;
    }

    public int peek() {
        if(isEmpty()) {
            throw new EmptyException("栈为空...");
        }
        return elem[usedSize - 1];
    }
}
