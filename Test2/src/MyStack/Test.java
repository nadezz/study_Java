package MyStack;

public class Test {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(12);
        myStack.push(23);
        myStack.push(34);
        myStack.push(45);

        int ret1 = myStack.pop();
        System.out.println(ret1);

        int ret2 = myStack.peek();
        System.out.println(ret2);
    }
}
