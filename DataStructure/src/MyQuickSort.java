
public class MyQuickSort {
    public static void quickSort(int[] arr, int left, int right) {
        if(left < right) {
            //因为有了随机标志数，所以才会是O(N*longN)
            swap(arr,left + (int)(Math.random() * (right - left + 1)),right);
            int[] p = partition(arr,left,right);
            quickSort(arr,left,p[0] - 1);
            quickSort(arr,p[1] + 1,right);
        }
    }

    public static int[] partition(int[] arr, int L, int R) {
        int key = arr[R]; //标志数
        int less = L - 1;
        int more = R;
        while(L < more) {
            if(arr[L] < key) {
                swap(arr,L,less + 1);
                less++;
                L++;
            } else if(arr[L] == key) {
                L++;
            } else {
                swap(arr,L,more - 1);
                more--;
            }
        }
        swap(arr,more,R);
        return new int[] {less + 1, more};
    }

    public static void swap(int[] arr, int a, int b) {
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = {5,4,2,7,95,1,8,23};
        quickSort(arr,0,arr.length-1);
        for(int x : arr) {
            System.out.print(x+" ");
        }
        System.out.println();
    }
}
