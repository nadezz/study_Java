public class RadixSort {

    public static void radixSort(int[] arr, int left, int right, int digit) {
        int[] helpArr = new int[right - left + 1];   //辅助数组
        int j = 0;
        for (int k = 1; k <= digit; k++) {
            int[] count = new int[10];  //前缀和数组
            for (int i = left; i <= right; i++) {
                j = getDigit(arr[i],k);
                count[j]++;
            }
            //计算出前缀和
            for (int i = 1; i < 10; i++) {
                count[i] = count[i - 1] + count[i];
            }
            //将排序最终结果放到辅助数组中
            for (int i = right; i >= left; i--) {
                j = getDigit(arr[i],k);
                helpArr[count[j] - 1] = arr[i];
                count[j]--;
            }
            j = 0;
            //将辅助数组拷贝回原数组
            for (int i = left; i <= right; i++) {
                arr[i] = helpArr[j++];
            }
        }
    }

    //计算有几位数
    public static int getMaxBits(int[] arr) {
        int max = Integer.MIN_VALUE;
        int ret = 0;
        for (int x : arr) {
            max = Math.max(max, x);
        }
        while(max != 0) {
            ret++;
            max /= 10;
        }
        return ret;
    }

    //计算出a的第k位的数字
    public static int getDigit(int a, int k) {
        return (a / ((int)Math.pow(10,k - 1))) % 10;
    }

    public static void main(String[] args) {
        int[] arr = {5,4,2,7,95,1,8,23};
        radixSort(arr,0,arr.length - 1,getMaxBits(arr));
        for(int x : arr) {
            System.out.print(x+" ");
        }
        System.out.println();
    }
}
