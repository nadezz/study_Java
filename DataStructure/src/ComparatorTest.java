import java.util.*;




public class ComparatorTest {

    static class Student {
        public String name;
        public int age;

        public Student(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    static class AgeComparator implements Comparator<Student> {

        @Override
        public int compare(Student o1, Student o2) {
            return o2.age - o1.age;
        }
    }

    public static void main(String[] args) {
        Student student1 = new Student("小明",4);
        Student student2 = new Student("小红",1);
        Student student3 = new Student("张三",3);

        Student[] students = {student1,student2,student3};


        //方法1
        Arrays.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o2.age - o1.age;
            }
        });

        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }

        //方法2
        Arrays.sort(students, new AgeComparator());
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }
    }
}
