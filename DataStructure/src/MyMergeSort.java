public class MyMergeSort {
    public static void MergeProcess(int[] arr, int left, int right) {
        if(left == right) {
            return;
        }
        int mid = left + (right - left) / 2;
        MergeProcess(arr,left,mid);
        MergeProcess(arr,mid + 1,right);
        MergeSort(arr,left,mid,right);
    }

    public static void MergeSort(int[] arr, int left, int mid, int right) {
        int[] helpArr = new int[right - left + 1];
        int p1 = left;
        int p2 = mid + 1;
        int i = 0;
        while(p1 <= mid && p2 <= right) {
            helpArr[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        while(p1 <= mid) {
            helpArr[i++] = arr[p1++];
        }
        while(p2 <= right) {
            helpArr[i++] = arr[p2++];
        }
        for(i = 0 ; i < helpArr.length; i++) {
            arr[left + i] = helpArr[i];
        }
    }

    public static void main(String[] args) {
        int[] arr = {5,4,2,7,95,1,8,23};
        MergeProcess(arr,0,arr.length-1);
        for(int x : arr) {
            System.out.print(x+" ");
        }
        System.out.println();
    }
}
