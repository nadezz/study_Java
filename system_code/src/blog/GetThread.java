package blog;

class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("MyThread :"+this.getName());
    }
}
public class GetThread {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            Thread thread = Thread.currentThread();
            System.out.println("t1线程中: "+thread.getName());

        });
        Thread t2 = new MyThread();

        t1.start();
        t1.join();

        t2.start();
    }
}
