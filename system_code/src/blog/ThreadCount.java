package blog;

public class ThreadCount {
    private static int count = 0;

    public static void main(String[] args) throws InterruptedException {
        // 随便创建个对象都行
        Object locker = new Object();

        // 创建两个线程. 每个线程都针对上述 count 变量循环自增 50w 次
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 500000; i++) {
                synchronized (locker) {
                    count++;
                }
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 500000; i++) {
                synchronized (locker) {
                    count++;
                }
            }
        });
        t1.start();
        t2.start();

        t1.join();
        t2.join();
        System.out.println("count = " + count);
    }
}
