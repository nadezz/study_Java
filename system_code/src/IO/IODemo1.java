package IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class IODemo1 {
    public static void main(String[] args) throws IOException {
//        InputStream inputStream = new FileInputStream("./src/IO/test.txt");
//
//        inputStream.close();

        //1. 普通写法
//        InputStream inputStream = null;
//        try {
//            inputStream = new FileInputStream("./src/IO/test.txt");
//        } finally {
//            inputStream.close();
//        }

        //2. 实现了Closeable 接口的类，才可以使用放到 try（）里，即 try with resources
        try (InputStream inputStream = new FileInputStream("./src/IO/test.txt")) {
            //读文件
            byte[] buffer = new byte[100];
            int n = inputStream.read(buffer);
            if (n == -1) {
                return;
            }
            System.out.println(Arrays.toString(buffer));
        }
    }
}
