package IO;

import java.io.*;
import java.util.Scanner;

public class CopyFile {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要复制的源文件：");
        String srcPath = sc.nextLine();
        System.out.println("请输入要复杂的目标文件：");
        String destPath = sc.nextLine();

        File srcFile = new File(srcPath);
        if (!srcFile.isFile()) {
            System.out.println("源文件路径不正确！");
            return;
        }
        File destFile = new File(destPath);
        if (!destFile.getParentFile().isDirectory()) {
            System.out.println("目标文件路径不正确");
            return;
        }

        try (InputStream inputStream = new FileInputStream(srcFile);
                OutputStream outputStream = new FileOutputStream(destFile)) {
            while (true) {
                byte[] buffer = new byte[1000];
                int n = inputStream.read(buffer);
                if (n == -1) {
                    break;
                }
                outputStream.write(buffer,0,n);
            }
            System.out.println("复制完毕！");
        }
    }
}
