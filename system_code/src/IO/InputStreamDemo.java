package IO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamDemo {
    public static void main(String[] args) throws IOException {
        try (InputStream inputStream = new FileInputStream("./src/IO/test.txt")) {
            //读文件
            byte[] buffer = new byte[100];
            int n = inputStream.read(buffer);
            if (n == -1) {
                return;
            }
            // 基于字符数组构造 String
            String s = new String(buffer,0,n);
            System.out.println(s);
        }
    }
}
