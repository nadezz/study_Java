package IO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo {
    public static void main(String[] args) throws IOException {
        try (OutputStream outputStream = new FileOutputStream("./src/IO/test.txt",true)) {
            byte[] buffer = {97, 98, 99, 100, 101, 102};
            outputStream.write(buffer,0,buffer.length);
        }
    }
}
