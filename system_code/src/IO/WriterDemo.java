package IO;

import java.io.*;

public class WriterDemo {
    public static void main(String[] args) throws IOException {
        try (Writer writer = new FileWriter("./src/IO/test.txt",true)){
            String s = "你好啊";
            writer.write(s);
        }
    }
}
