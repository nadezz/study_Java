package IO;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class ReaderDemo {
    public static void main(String[] args) throws IOException {
        // 使用字符流读取文件内容
        try (Reader reader = new FileReader("./src/IO/test.txt")){
            // 以字符为单位读取，因此数组类型是 char
            char[] buffer = new char[1000];
            int n = reader.read(buffer);
            if (n == -1) {
                return;
            }
            String s = new String(buffer,0,n);
            System.out.println(s);
        }
    }
}
