package IO;

import java.io.*;
import java.util.Scanner;

public class SearchFileByContent {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要搜索的路径：");
        String searchPath = sc.nextLine();
        System.out.println("请输入要搜索的词：");
        String word = sc.nextLine();

        File file = new File(searchPath);
        if (!file.exists()) {
            System.out.println("路径不正确！");
            return;
        }

        scanDir(file,word);
    }

    public static void scanDir(File file, String word) {
        File[] files = file.listFiles();
        if (files == null) {
            return;
        }
        for (File f : files) {
            if (f.isFile()) {
                searchWord(f,word);
            } else if (f.isDirectory()) {
                scanDir(f,word);
            }
        }
    }

    private static void searchWord(File f, String word) {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream = new FileInputStream(f)) {
            while (true) {
                byte[] buffer = new byte[1000];
                int n = inputStream.read(buffer);
                if (n == -1) {
                    break;
                }
                String s = new String(buffer,0,n);
                stringBuilder.append(s);
            }

            if (stringBuilder.indexOf(word) == -1) {
                return;
            }
            System.out.println("找到含有 "+ word +" 的文件路径：" + f.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
