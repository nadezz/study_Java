package IO;

import java.io.File;
import java.net.FileNameMap;
import java.util.Scanner;

public class SearchFile {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要搜索的路径：");
        String searchPath = sc.nextLine();
        System.out.println("请输入要查找的文件名：");
        String fileName = sc.nextLine();

        File rootFile = new File(searchPath);
        if (!rootFile.exists()) {
            System.out.println("搜索路径不正确！");
            return;
        }
        scanDir(rootFile,fileName);
    }

    public static void scanDir(File rootFile, String fileName) {
        File[] files = rootFile.listFiles();
        if (files == null) {
            //空目录返回
            return;
        }
        for (File file : files) {
            System.out.println("当前遍历到：" + file.getAbsolutePath());
            if (file.isFile()) {
                if (file.getName().equals(fileName)) {
                    System.out.println("找到该文件，路径为：" + file.getAbsolutePath());
                }
            } else if (file.isDirectory()) {
                scanDir(file, fileName);
            }
        }
    }
}
