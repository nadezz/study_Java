package MyTimer;

import java.util.PriorityQueue;

class MyTimer2Task implements Comparable<MyTimer2Task> {
    private long time;
    private Runnable runnable;

    public MyTimer2Task(Runnable runnable, long time) {
        this.runnable = runnable;
        this.time = System.currentTimeMillis() + time;
    }

    public long getTime() {
        return this.time;
    }

    public void run() {
        runnable.run();
    }

    @Override
    public int compareTo(MyTimer2Task o) {
        return (int) (this.time - o.time);
    }
}

public class MyTimer2 {

    private Thread t = null;

    private PriorityQueue<MyTimer2Task> queue = new PriorityQueue<>();

    private Object locker = new Object();

    public void schedule(Runnable runnable, long delay) {
        synchronized (locker) {
            queue.offer(new MyTimer2Task(runnable, delay));
            locker.notify();
        }
    }

    public MyTimer2() {
        t = new Thread(() -> {
            while (!t.isInterrupted()) {
                try {
                    synchronized (locker) {
                        while (queue.isEmpty()) {
                            //阻塞
                            locker.wait();
                        }
                        MyTimer2Task task = queue.peek();
                        long currentTime = System.currentTimeMillis();
                        if (currentTime >= task.getTime()) {
                            queue.poll();
                            task.run();
                        } else {
                            locker.wait(task.getTime() - currentTime);
                        }
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();
    }

    public void cancel() {
        t.interrupt();
    }
}
