package MyTimer;

import java.util.PriorityQueue;

class MyTimerTask implements Comparable<MyTimerTask> {
    //time 时一个 ms 级别的时间戳
    private long time;
    //实际任务要执行的代码，由于Runnable中带有run方法，可以再run中写要执行的代码
    private Runnable runnable;

    public MyTimerTask(Runnable runnable, long delay) {
        this.runnable = runnable;
        this.time = System.currentTimeMillis() + delay; // delay 是相对时间
    }

    public long getTime() {
        return this.time;
    }

    public void run() {
        runnable.run();
    }

    @Override
    public int compareTo(MyTimerTask o) {
        return (int) (this.time - o.time);
    }
}

public class MyTimer {
    //负责扫描任务队列，执行任务的线程
    private Thread t = null;
    //任务队列
    private PriorityQueue<MyTimerTask> queue = new PriorityQueue<>();

    private Object locker = new Object();

    public void schedule(Runnable runnable, long delay) {
        synchronized (locker) {
            MyTimerTask task = new MyTimerTask(runnable, delay);
            queue.offer(task);
            // 添加新元素后，唤醒扫描线程的 wait
            locker.notify();
        }
    }

    //构造方法
    public MyTimer() {
        //扫描、执行任务线程
        t = new Thread(() -> {
            while (!t.isInterrupted()) {
                try {
                    synchronized (locker) {
                        while (queue.isEmpty()) {  //wait 搭配 while 使用
                            locker.wait();
                        }
                        MyTimerTask task = queue.peek();
                        long currentTime = System.currentTimeMillis();
                        if (currentTime >= task.getTime()) {
                            queue.poll();
                            task.run();
                        } else {
                            // 注意此处写法，不能使用sleep 【23/12/4 20:00】
                            locker.wait(task.getTime() - currentTime);
                        }
                    }  //解锁
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        // 容易忘记启动线程，记得 start
        t.start();
    }

    public void cancel () {
        // interrupt 结束线程
        t.interrupt();
    }
}
