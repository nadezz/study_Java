package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TcpEchoServer {
    private ServerSocket serverSocket = null;

    public TcpEchoServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        System.out.println("服务器启动！");
        // 使用线程池减少线程反复创建销毁的开销
        ExecutorService pool = Executors.newCachedThreadPool();
        while (true) {
            // 当客户端创建出 socket 后（new socket），就会和对应的服务器进行 tcp 连接建立流程
            // 此时通过 accept 方法来“接听电话”，然后才能进行通信
            Socket clientSocket = serverSocket.accept();
            // 使用线程解决多客户端同时连接的问题
//            Thread t = new Thread(() -> {
//                processConnection(clientSocket);
//            });
//            t.start();

            // 使用线程池减少开销
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    processConnection(clientSocket);
                }
            });
        }
    }

    private void processConnection(Socket clientSocket) {
        System.out.printf("[%s:%d] 客户端上线！\n", clientSocket.getInetAddress(), clientSocket.getPort());
        // 循环读取客户端的请求并返回响应
        try (InputStream inputStream = clientSocket.getInputStream();
             OutputStream outputStream = clientSocket.getOutputStream()){
            // 可以使用 inputStream 原本的 read 方法进行读取
            // 但是比较繁琐，为了【方便读入]，这里使用 Scanner 对输入流进行输入
            Scanner sc = new Scanner(inputStream);
            while (true) {
                if (!sc.hasNext()) {
                    // 读取完毕，客户端断开连接
                    System.out.printf("[%s:%d] 客户端下线！\n", clientSocket.getInetAddress(), clientSocket.getPort());
                    break;
                }
                //1. 读取请求并解析，此处使用 next ，需要注意 next 的读入规则
                String request = sc.next();
                //2. 根据请求计算响应
                String response = process(request);
                //3. 把响应返回给客户端

                /*  通过这种方式也可以写回，但是这种方式不方便添加 \n
                outputStream.write(response.getBytes(),0,response.getBytes().length);*/

                // 因此为了【方便写入】，给 outputStream 也套一层，即使用 printWriter
                // 此处的 printWriter 就类似于 Scanner 将输入流包装了一下，而 printWriter 对输出流包装了一下
                PrintWriter printWriter = new PrintWriter(outputStream);
                // 通过 println 在末尾添加了 \n，与客户端的 scNetwork.next 呼应
                printWriter.println(response);
                // 刷新缓冲区，确保数据能够发送出去 （24/1/23 11:50）
                printWriter.flush();

                // 打印日志
                System.out.printf("[%s:%d] req: %s, resp: %s\n", clientSocket.getInetAddress(), clientSocket.getPort(),
                        request,response);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            // 每一个客户端分配一个“置业顾问”，因此每个客户端完成连接后需要关闭“置业顾问”
            try {
                clientSocket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }

    private String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        TcpEchoServer server = new TcpEchoServer(9090);
        server.start();
    }
}
