package network;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;

public class UdpDicServer extends UdpEchoServer{

    private HashMap<String, String> hashMap = new HashMap<>();

    public UdpDicServer(int port) throws SocketException {
        super(port);

        hashMap.put("cat", "小猫");
        hashMap.put("dog", "小狗");
        hashMap.put("chicken", "小鸡");
    }

    // start 方法完全从父类继承下来即可
    // 重写 process，加入新的业务逻辑，翻译操作
    @Override
    public String process(String request) {
        return hashMap.getOrDefault(request,"查找的单词不存在！");
    }

    public static void main(String[] args) throws IOException {
        UdpDicServer server = new UdpDicServer(9090);
        server.start();
    }
}
