package network;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class UdpEchoClient {

    private DatagramSocket socket = null;

    private String serverIp;
    private int serverPort;

    public UdpEchoClient(String serverIp, int serverPort) throws SocketException {
        // 客户端，正常情况下不需要指定端口【饭店地址号和餐桌号的区别】
        socket = new DatagramSocket();
        this.serverIp = serverIp;
        this.serverPort = serverPort;  // 客户端对应的服务器端口号
    }

    public void start() throws IOException {
        System.out.println("客户端启动");
        Scanner sc = new Scanner(System.in);
        while (true) {
            //1. 从控制台读取要发送的数据
            System.out.print("-> "); //表示提示用户输入
            if (!sc.hasNext()) {   //hasNext 具有阻塞功能
                break;
            }
            String request = sc.next();
            //2. 构造请求并发送
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(), request.getBytes().length,
                    InetAddress.getByName(serverIp), serverPort);
            socket.send(requestPacket);
            //3. 读取服务器的响应
            DatagramPacket responsePacket = new DatagramPacket(new byte[4096],4096);
            // 阻塞等待响应数据返回
            socket.receive(responsePacket);
            //4. 把响应显示到控制台
            String response = new String(responsePacket.getData(),0,responsePacket.getLength());
            System.out.println(response);
        }
    }

    public static void main(String[] args) throws IOException {
        UdpEchoClient client = new UdpEchoClient("127.0.0.1", 9090);
        client.start();
    }
}
