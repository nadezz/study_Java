package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UdpEchoServer {

    private DatagramSocket socket = null;

    public UdpEchoServer(int port) throws SocketException {
        // 服务器需要指定端口号【饭店地址号和餐桌号的区别】
        socket = new DatagramSocket(port);
    }

    public void start() throws IOException {
        System.out.println("服务器启动");
        while (true) {
            //每次循环，都是一次处理请求，进行响应的过程
            //1. 读取请求并解析
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096], 4096);
            socket.receive(requestPacket);
            // 将读到的字节数组转换成 String 方便后续操作
            String request = new String(requestPacket.getData(),0,requestPacket.getLength());
            //2. 根据请求计算响应
            String response = process(request);
            //3. 把响应返回到客户端
            // 与请求数据报创建不同，请求数据报是使用空白字节数组，而此处直接把 String 里包含的字节数组作为参数创建，
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(), response.getBytes().length,
                    requestPacket.getSocketAddress());  // 因为 UDP 无连接，因此必须从【请求数据报】中获取对应客户端的 ip 和端口
            socket.send(responsePacket);

            //打印日志
            System.out.printf("[%s:%d] req: %s, resp: %s\n",requestPacket.getAddress().toString(),
                    requestPacket.getPort(), request, response);
        }
    }

    public String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer server = new UdpEchoServer(9090);
        server.start();
    }
}
