package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TcpEchoClient {

    private Socket socket = null;

    public TcpEchoClient(String serverIp, int serverPort) throws IOException {
        // 这里直接将 ip 和 port 传入，是由于 tcp 是有连接的，socket 里能够保存 ip 和 port
        socket = new Socket(serverIp,serverPort);
        // 因此也不需要额外创建【类成员对象】来保存 ip 和 port
    }

    public void start() {
        System.out.println("客户端启动！");
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()){
            // 此处的 scanner 用于控制台读取数据
            Scanner scConsole = new Scanner(System.in);
            // 此处的 scanner 用于读取服务器响应回来的数据
            Scanner scNetwork = new Scanner(inputStream);
            // 此处 printWriter 用于向服务器写入请求数据
            PrintWriter printWriter = new PrintWriter(outputStream);
            while (true) {
                // 这里流程和 UDP 的客户端类似
                //1. 从控制台读取输入的字符串
                System.out.print("-> ");
                if (!scConsole.hasNext()) {
                    break;
                }
                String request = scConsole.next();
                //2. 把请求发送给服务器，
                // 使用 printWriter 是为了使发送的请求末尾带有 \n，与服务器的 sc.next 呼应
                printWriter.println(request);
                // 刷新缓冲区，确保数据能够发送出去 （24/1/23 11:50）
                printWriter.flush();
                //3. 从服务器读取响应
                String response = scNetwork.next();
                //4. 打印响应
                System.out.println(response);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException {
        TcpEchoClient client = new TcpEchoClient("127.0.0.1", 9090);
        client.start();
    }
}
