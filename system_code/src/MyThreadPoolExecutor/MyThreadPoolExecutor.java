package MyThreadPoolExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MyThreadPoolExecutor {
    private List<Thread> threadList = new ArrayList<>();

    // 保存任务的队列
    private BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(1000);

    public MyThreadPoolExecutor(int n) {  //创建n个线程
        for (int i = 0; i < n; i++) {
            Thread t = new Thread(() -> {
                //这些线程需要做的就是不断从任务队列中取出任务并执行
                while (true) {
                    try {
                        //此时的 take 带有阻塞功能，为空时自动阻塞
                        Runnable runnable = queue.take();
                        runnable.run();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            t.start();
            threadList.add(t);
        }
    }

    public void submit(Runnable runnable) throws InterruptedException {
        queue.put(runnable);
    }
}
