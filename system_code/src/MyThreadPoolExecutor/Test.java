package MyThreadPoolExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
    public static void main(String[] args) throws InterruptedException {
//        【【【【Java 标准库的线程池】】】
//        ExecutorService service = Executors.newFixedThreadPool(4);
//        service.submit(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("hello threadPool");
//            }
//        });
        MyThreadPoolExecutor service = new MyThreadPoolExecutor(4);
        for (int i = 0; i < 1000; i++) {
            int n = i;
            service.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("执行任务"+ n + " ， 当前线程为： " + Thread.currentThread().getName());
                }
            });
        }
    }
}
