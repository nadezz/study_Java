package thread;

public class SynchronizedDemo3 {
    public static void main(String[] args) {
        Object lockerA = new Object();
        Object lockerB = new Object();

        Thread t1 = new Thread(() -> {
            synchronized (lockerA) {
                System.out.println("t1:lockerA");
                synchronized (lockerB) {
                    System.out.println("t1:lockerB");
                }
            }
        });
        Thread t2 = new Thread(() -> {
            synchronized (lockerB) {
                System.out.println("t2:lockerB");
                synchronized (lockerA) {
                    System.out.println("t2:lockerA");
                }
            }
        });
        t1.start();
        t2.start();

    }
}
