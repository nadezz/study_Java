package thread;

public class ThreadDemo5 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(new Runnable() {   //实现Runnable接口的匿名内部类，这里的写法搭配Demo3理解
            @Override
            public void run() {
                while (true) {
                    System.out.println("hello runnable");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        t.start();

        while (true) {
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
