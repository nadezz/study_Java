package thread;

public class ThreadDemo6 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {   //实现Runnable接口重写run的lambda写法【推荐使用】
            while (true) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t.start();

        while (true) {
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }

}
