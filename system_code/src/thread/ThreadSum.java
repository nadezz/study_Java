package thread;

import java.util.Random;

public class ThreadSum {

    public static long sum = 0;
    public static void main(String[] args) throws InterruptedException {
        int[] nums = new int[10_000_000];
        Random random = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = random.nextInt(100) + 1;
        }
        Thread t1 = new Thread(() -> {
            int tmp = 0;
            for (int i = 0; i < nums.length; i+=2) {
                tmp += nums[i];
            }
            sum += tmp;
        });
        Thread t2 = new Thread(() -> {
            int tmp = 0;
            for (int i = 1; i < nums.length; i+=2) {
                tmp += nums[i];
            }
            sum += tmp;
        });
        long start = System.currentTimeMillis();
        t1.start();
        t2.start();

        t1.join();
        t2.join();
        long end = System.currentTimeMillis();

        System.out.println("sum = "+sum);
        System.out.println("time = "+(end - start)+" ms");
    }

    //用于比较
    public static void main1(String[] args) {
        int[] nums = new int[10_000_000];
        Random random = new Random();
        for (int i = 0; i < nums.length; i++) {
            nums[i] = random.nextInt(100) + 1;
        }
        long start = System.currentTimeMillis();
        long sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        long end = System.currentTimeMillis();
        System.out.println(sum);
        System.out.println(end - start);
    }
}
