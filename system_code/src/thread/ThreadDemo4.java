package thread;

public class ThreadDemo4 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread() {   //继承thread类的匿名内部类
            @Override
            public void run() {
                while (true) {
                    System.out.println("hello thread");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        t.start();

        while (true) {
            System.out.println("hello main");
            Thread.sleep(1000);
        }

    }
}
