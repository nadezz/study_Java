package thread;

import java.util.Scanner;

public class ThreadDemo7 {
    private static volatile int flag = 0;

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            while (flag == 0) {
                // 循环体里, 啥都不写才会触发内存可见性问题
                // 如果有内容则不会发生
            }
            System.out.println("t1 线程结束!");
        });

        Thread t2 = new Thread(() -> {
            System.out.println("请输入 flag 的值: ");
            Scanner scanner = new Scanner(System.in);
            flag = scanner.nextInt();
        });

        t1.start();
        t2.start();
    }
}