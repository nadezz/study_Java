package thread;

public class ThreadInterrupt {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;   //注意此处需要添加break，因为sleep会清空标志位，详情可见23/11/22 20:25
                }
            }
            System.out.println("线程执行完毕");
        });
        t.start();
        Thread.sleep(3000);
        t.interrupt();
    }
}
