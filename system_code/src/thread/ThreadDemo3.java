package thread;

class MyThread3 implements Runnable {   //实现Runnable接口
    @Override
    public void run() {
        while (true) {
            System.out.println("hello runnable");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

public class ThreadDemo3 {
    public static void main(String[] args) throws InterruptedException {
        Runnable runnable = new MyThread3();
        Thread t = new Thread(runnable);
        t.start();

        while (true) {
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
