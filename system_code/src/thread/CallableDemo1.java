package thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CallableDemo1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> callable = new Callable<>() {
            @Override
            public Integer call() throws Exception {
                int ret = 0;
                for (int i = 1; i <= 1000; i++) {
                    ret += i;
                }
                return ret;
            }
        };

        //引入 FutureTask 类，作为 Thread 和 callable 的 “粘合剂”
        FutureTask<Integer> futureTask = new FutureTask<>(callable);
        Thread t = new Thread(futureTask);
        t.start();

        //取餐号，使用 get() 获取到返回结果，具有阻塞功能
        System.out.println(futureTask.get());
    }
}
