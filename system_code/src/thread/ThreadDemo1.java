package thread;

//1. 创建一个自己的类，继承Thread线程类
class MyTread extends Thread {
    @Override
    public void run() {
        // run方法是该线程的入口方法，类比main方法是java进程的入口方法一样
        System.out.println("hello world");
    }
}
public class ThreadDemo1 {
    public static void main(String[] args) {
        //2. 创建出类的实例（线程实例，才是真正的线程）
        Thread t = new MyTread();
        //3. 调用Thread的start方法，才会真正调用系统api，在系统内核中创建出线程
        t.start();  //自动执行run方法
    }
}
