package thread;

class MyThread2 extends Thread {
    @Override
    public void run() {   //重写的时候，父类如果没有throws这个异常，子类就无法throws这个异常，否则无法构成“重写”
        while (true) {
            System.out.println("hello thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
public class ThreadDemo2 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new MyThread2();
        t.start();

        while (true) {
            System.out.println("hello main");
            Thread.sleep(1000);
        }
    }
}
