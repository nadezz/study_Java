package thread;

public class SynchronizedDemo1 {

    public static long count = 0;
    public static void main(String[] args) throws InterruptedException {
        Object locker = new Object();   //锁对象，可以是任意类型的

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronized (locker) {   //加锁
                    count++;
                }
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronized (locker) {   //加锁
                    count++;
                }
            }
        });
        t1.start();
        t2.start();

        t1.join();
        t2.join();
        System.out.println(count);
    }
}
