package MyBlockingQueue;

public class MyBlockingQueue {
    private String[] elems = null;
    private int head = 0;
    private int tail = 0;
    private int UsedSize = 0;

    private Object locker = new Object();

    public MyBlockingQueue(int capacity) {
        elems = new String[capacity];
    }

    public void put(String elem) throws InterruptedException {
        synchronized (locker) {
            while (UsedSize >= elems.length) {   //被唤醒后此时可能又已经满了，因此需要将if改为while，
                                              //表示当wait被唤醒后再判定一次条件，如果满了再次进入阻塞
                //阻塞
                locker.wait();
            }
            elems[tail] = elem;
            tail++;
            if (tail >= elems.length) {
                tail = 0;
            }
            UsedSize++;
            //入队成功后，唤醒空队列的wait
            locker.notify();
        }
    }

    public String take() throws InterruptedException {
        String elem = null;
        synchronized (locker) {
            while (UsedSize == 0) {    //同理，if改为while
                //阻塞
                locker.wait();
            }
            elem = elems[head];
            head++;
            if (head >= elems.length) {
                head = 0;
            }
            UsedSize--;
            //出队成功后，唤醒满队列的wait
            locker.notify();
        }
        return elem;
    }
}
