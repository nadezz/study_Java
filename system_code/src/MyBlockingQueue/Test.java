package MyBlockingQueue;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        MyBlockingQueue queue = new MyBlockingQueue(1000);
//        queue.put("aaa");
//        queue.put("bbb");
//        queue.put("ccc");
//        queue.put("ddd");
//
//        String elem = queue.take();
//        System.out.println(elem);
//        elem = queue.take();
//        System.out.println(elem);
//        elem = queue.take();
//        System.out.println(elem);
//        elem = queue.take();
//        System.out.println(elem);
        //生产者
        Thread t1 = new Thread(() -> {
            int n = 1;
            while (true) {
                try {
                    queue.put(n+"");
                    System.out.println("生产元素 "+n);
                    n++;
                    //Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        //消费者
        Thread t2 = new Thread(() -> {
            while (true) {
                try {
                    String n = queue.take();
                    System.out.println("消费元素 "+n);
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        t1.start();
        t2.start();
    }
}
