package hw;

public class OrderPrintPlus {
    public static void main(String[] args) {
        Object lockerA = new Object();
        Object lockerB = new Object();
        Object lockerC = new Object();

        Thread a = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                synchronized (lockerC) {
                    try {
                        System.out.print("A");
                        synchronized (lockerA) {
                            lockerA.notify();
                        }
                        lockerC.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        Thread b = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                synchronized (lockerA) {
                    try {
                        lockerA.wait();
                        System.out.print("B");
                        synchronized (lockerB) {
                            lockerB.notify();
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        Thread c = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                synchronized (lockerB) {
                    try {
                        lockerB.wait();
                        System.out.println("C");
                        synchronized (lockerC) {
                            lockerC.notify();
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        a.start();
        b.start();
        c.start();
    }
}
