package hw;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;

public class UdpDicServer extends UdpEchoServer{

    private HashMap<String,String> hashMap = new HashMap<>();

    public UdpDicServer(int port) throws SocketException {
        super(port);

        hashMap.put("小狗","dog");
        hashMap.put("小猫","cat");
        hashMap.put("小鸡","chicken");
    }

    @Override
    public String process(String request) {
        return hashMap.getOrDefault(request,"查无此的单词");
    }

    public static void main(String[] args) throws IOException {
        UdpDicServer server = new UdpDicServer(9090);
        server.start();
    }
}
