package hw;

import java.io.IOException;
import java.util.HashMap;

public class TcpDicServer extends TcpEchoServer{

    private HashMap<String,String> hashMap = new HashMap<>();

    public TcpDicServer(int port) throws IOException {
        super(port);

        hashMap.put("dog","小狗");
        hashMap.put("cat","小猫");
        hashMap.put("chicken","小鸡");
        hashMap.put("rabbit","兔子");
        hashMap.put("monkey","猴子");
        hashMap.put("book","书本");
    }

    @Override
    public String process(String request) {
        return hashMap.getOrDefault(request,"查找的单词不存在！");
    }

    public static void main(String[] args) throws IOException {
        TcpDicServer server = new TcpDicServer(9090);
        server.start();
    }
}
