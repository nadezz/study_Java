package hw;

class MyBlockingQueue {
    private String[] elems = null;
    private int UsedSize = 0;
    private int head = 0;
    private int tail = 0;
    private Object locker = new Object();
    public MyBlockingQueue(int capacity) {
        elems = new String[capacity];
    }

    public void put(String elem) throws InterruptedException {
        synchronized (locker) {
            while (UsedSize >= elems.length) {
                //阻塞
                locker.wait();
            }
            elems[tail] = elem;
            tail++;
            if (tail == elems.length) {
                tail = 0;
            }
            UsedSize++;
            locker.notify();
        }
    }

    public String tack() throws InterruptedException {
        String ret = null;
        synchronized (locker) {
            while (UsedSize == 0) {
                //阻塞
                locker.wait();
            }
            ret = elems[head];
            head++;
            if (head == elems.length) {
                head = 0;
            }
            UsedSize--;
            locker.notify();
        }
        return ret;

    }
}
public class MBQ {

    public static void main(String[] args) {
        MyBlockingQueue queue = new MyBlockingQueue(1000);

        //生产者
        Thread t1 = new Thread(() -> {
            int n = 1;
            while (true) {
                try {
                    queue.put(n+"");
                    System.out.println("生产元素 "+n);
                    n++;
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        //消费者
        Thread t2 = new Thread(() -> {
            while (true) {
                try {
                    String n = queue.tack();
                    System.out.println("消费元素 "+n);
                    //Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
        });
        t1.start();
        t2.start();
    }
}
