package hw;

public class OrderPrint {
    public static void main(String[] args) {
        Object lockerA = new Object();
        Object lockerB = new Object();

        Thread a = new Thread(() -> {
            synchronized (lockerB) {
                try {
                    lockerB.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("a Thread");
            }

        });
        Thread b = new Thread(() -> {
            synchronized (lockerA) {
                try {
                    lockerA.wait();
                    System.out.println("b Thread");
                    synchronized (lockerB) {
                        lockerB.notify();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        Thread c = new Thread(() -> {
            System.out.println("c Thread");
            synchronized (lockerA) {
                lockerA.notify();
            }
        });
        a.start();
        b.start();
        c.start();
    }
}
