package hw;

import java.io.File;
import java.util.Scanner;

public class DeleteFileByName {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要搜索的路径：");
        String searchPath = sc.nextLine();
        System.out.println("请输入要查找的文件名含有的字符：");
        String fileName = sc.nextLine();

        File rootFile = new File(searchPath);
        if (!rootFile.exists()) {
            System.out.println("搜索路径不正确！");
            return;
        }
        scanDirByName(rootFile,fileName);
    }

    public static void scanDirByName(File rootFile, String fileName) {
        File[] files = rootFile.listFiles();
        if (files == null) {
            //空目录返回
            return;
        }
        for (File file : files) {
            System.out.println("当前遍历到：" + file.getAbsolutePath());
            if (file.isFile()) {
                if (file.getName().contains(fileName)) {
                    Scanner sc = new Scanner(System.in);
                    System.out.println("找到文件名为："+ file.getName() +" 的文件，路径为：" + file.getAbsolutePath());
                    System.out.println("是否需要删除该文件？  1) 删除  0) 跳过");
                    if (sc.nextInt() == 1) {
                        file.delete();
                        System.out.println("删除 "+ file.getName() +" 成功！");
                    }
                }
            } else if (file.isDirectory()) {
                scanDirByName(file, fileName);
            }
        }
    }
}
