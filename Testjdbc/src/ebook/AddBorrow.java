package ebook;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddBorrow {
    public static void main(String[] args) throws SQLException, ParseException {
        Date Startdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-09-25 17:50:00");
        Date Enddate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-10-25 17:50:00");
        Timestamp start = new Timestamp(Startdate.getTime());
        Timestamp end = new Timestamp(Enddate.getTime());

        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://localhost:3306/ebook");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root123");

        Connection connection = dataSource.getConnection();
        String sql = "insert into borrow_info (book_id,student_id,start_time,end_time) " +
                "select book.id,student.id,?,? from student,book where student.name = ? and book.name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setTimestamp(1,start);
        statement.setTimestamp(2,end);
        statement.setString(3,"貂蝉");
        statement.setString(4,"诗经");


        int i = statement.executeUpdate();
        System.out.println("i = "+i);

        statement.close();
        connection.close();
    }
}
