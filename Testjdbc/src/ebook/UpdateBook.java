package ebook;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateBook {
    public static void main(String[] args) throws SQLException {
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://localhost:3306/ebook");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root123");

        Connection connection = dataSource.getConnection();
        String sql = "update book set price = ? where name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setDouble(1,61.30);
        statement.setString(2,"深入理解Java虚拟机");

        int i = statement.executeUpdate();
        System.out.println("i = "+i);

        statement.close();
        connection.close();
    }
}
