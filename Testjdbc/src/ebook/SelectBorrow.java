package ebook;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectBorrow {
    public static void main(String[] args) throws SQLException {
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://localhost:3306/ebook");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root123");

        Connection connection = dataSource.getConnection();
        String sql = "select book_id,student_id,start_time,end_time from book,category,borrow_info where book.category_id = category.id and " +
                "book.id = borrow_info.book_id and category.name = ? ";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1,"计算机");

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            int book_id = resultSet.getInt("book_id");
            int student_id = resultSet.getInt("student_id");
            String startTime = resultSet.getString("start_time");
            String endTime = resultSet.getString("end_time");
            System.out.println("书号："+book_id+" 借书人id："+student_id+" 开始时间："+startTime+" 结束时间"+endTime);
        }

        resultSet.close();
        statement.close();
        connection.close();
    }
}
