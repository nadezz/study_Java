package ebook;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeleteBorrow {
    public static void main(String[] args) throws SQLException {
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://localhost:3306/ebook");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root123");

        Connection connection = dataSource.getConnection();
        String sql1 = "select max(id) from borrow_info";
        PreparedStatement statement = connection.prepareStatement(sql1);
        ResultSet resultSet = statement.executeQuery();
        int maxId = 0;
        if (resultSet.next()) {
            maxId = resultSet.getInt("max(id)");
        }

        String sql2 = "delete from borrow_info where id = ?";
        statement = connection.prepareStatement(sql2);
        statement.setInt(1,maxId);
        int i = statement.executeUpdate();
        System.out.println("i = "+i);

        statement.close();
        connection.close();
    }
}
