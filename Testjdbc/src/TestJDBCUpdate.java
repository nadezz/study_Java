import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class TestJDBCUpdate {
    public static void main(String[] args) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请选择需要修改信息的姓名：");
        String name = sc.nextLine();
        System.out.println("请输入修改的姓名：");
        String Uname = sc.nextLine();
        System.out.println("请输入修改的年龄：");
        int Uage = sc.nextInt();

        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://localhost:3306/java110");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root123");

        Connection connection = dataSource.getConnection();
        String sql = "Update student set name = ?, age = ? where name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1,Uname);
        statement.setInt(2,Uage);
        statement.setString(3,name);

        int i = statement.executeUpdate();
        System.out.println("i = "+i);

        statement.close();
        connection.close();

    }
}
