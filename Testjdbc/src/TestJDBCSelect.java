import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestJDBCSelect {
    public static void main(String[] args) throws SQLException {
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://localhost:3306/java110");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("root123");

        Connection connection = dataSource.getConnection();

        String sql = "select * from student";
        PreparedStatement statement = connection.prepareStatement(sql);

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {  //遍历resultSet的典型写法，光标移动
            //获取 "name" 这一列的数值，期望得到一个 String
            String name = resultSet.getString("name");
            //获取 "age" 这一列的数值，期望得到一个 int
            int age = resultSet.getInt("age");
            System.out.println("name:" + name);
            System.out.println("age: "+ age);
        }

        //释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }
}
