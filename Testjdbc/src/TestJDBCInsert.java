import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class TestJDBCInsert {
    public static void main(String[] args) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入姓名：");
        String name = sc.nextLine();
        System.out.println("请输入年龄：");
        int age = sc.nextInt();


        //1. 创建数据源，数据源描述了要操作的数据库信息
        DataSource dataSource = new MysqlDataSource();  //向上转型
        ((MysqlDataSource) dataSource).setUrl("jdbc:mysql://localhost:3306/java110");
        ((MysqlDataSource) dataSource).setUser("root");
        ((MysqlDataSource) dataSource).setPassword("root123");

        //2. 通过数据源找到数据库位置，与数据库服务器建立连接
        Connection connection = dataSource.getConnection();

        //3. 构造一个SQL字符串
        String sql = "insert into student values(?, ?)";
        //将sql字符串转换成 语句对象
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1,name);   //此处还处于拼装sql语句阶段，未执行
        statement.setInt(2,age);

        //4. 把构造好的sql语句发送到服务器去【执行】
        int n = statement.executeUpdate();  //update返回int，Query返回ResultSet结果集
        System.out.println("n = " + n);

        //5.释放必要的资源
        statement.close();
        connection.close();

    }
}
