package com.java110.booksystem.controller;

import com.java110.booksystem.common.Constants;
import com.java110.booksystem.model.UserInfo;
import com.java110.booksystem.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 登录操作
     * @param userInfo
     * @param session
     * @return
     */
    @RequestMapping(value = "/login", produces = "application/json")   // 因为统一响应结果中字符串的返回被序列化了,为了让前端仍然认为它是 json 格式, 所以要手动设置以 json 返回
    public String login(UserInfo userInfo, HttpSession session) {
        //1. 校验参数
        if (!StringUtils.hasLength(userInfo.getUserName())
                || !StringUtils.hasLength(userInfo.getPassword())) {
            return "用户名或密码为空！";
        }
        //2. 根据用户名称查询用户信息，没查询到则说明不存在，查询到则对比密码是否正确
        UserInfo resultUser = userService.getUserInfoByName(userInfo.getUserName());
        //3. 返回响应结果
        if (resultUser == null) {
            return "用户不存在";
        }
        if (!userInfo.getPassword().equals(resultUser.getPassword())) {
            return "密码错误";
        }
        // 通过 session 返回信息
        session.setAttribute(Constants.USER_SESSION_KEY,userInfo);
        return "";
    }

}
