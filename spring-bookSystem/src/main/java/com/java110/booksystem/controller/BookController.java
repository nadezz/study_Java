package com.java110.booksystem.controller;

import com.java110.booksystem.common.Constants;
import com.java110.booksystem.common.Result;
import com.java110.booksystem.model.BookInfo;
import com.java110.booksystem.model.PageRequest;
import com.java110.booksystem.model.UserInfo;
import com.java110.booksystem.service.BookService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequestMapping("/book")
@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    /**
     * 带有分页查询的获取图书列表
     * @return
     */
    @RequestMapping("/getBookList")
    public Result getBookList(@RequestParam(defaultValue = "1") Integer page,   // 默认参数 1 和 5
                              @RequestParam(defaultValue = "5") Integer pageSize,
                              HttpSession session) {
        log.info("分页查询, 参数:{},{}",page,pageSize);

        PageRequest bookList = bookService.getBookList(page, pageSize);
        return Result.success(bookList);
    }

    /**
     * 添加图书
     * @param bookInfo
     * @return
     */
    @RequestMapping("/addBook")
    public Result insertBook(BookInfo bookInfo) {
        // 校验参数
        log.info("添加图书,接收到参数:{}",bookInfo);
        if (!StringUtils.hasLength(bookInfo.getBookName())
                || !StringUtils.hasLength(bookInfo.getAuthor())
                || bookInfo.getCount() == null
                || bookInfo.getPrice() == null
                || !StringUtils.hasLength(bookInfo.getPublish())
                || bookInfo.getStatus() == null ) {
            return Result.error("输入的参数不合法");
        }
        try {
            Integer result = bookService.insertBook(bookInfo);
            if (result == 0) {
                return Result.error("添加失败，请联系管理员！");
            }
        } catch (Exception e) {
            log.error("添加图书异常，e：",e);
        }
        return Result.success("");
    }

    /**
     * 根据id查询图书信息
     * @param bookId
     * @return
     */
    @RequestMapping("/queryBookById")
    public BookInfo queryBookById(Integer bookId) {
        log.info("根据id查询图书信息, id:{}", bookId);
        return bookService.queryBookById(bookId);
    }

    /**
     * 更新图书信息
     * @param bookInfo
     * @return
     */
    @RequestMapping(value = "/updateBook", produces = "application/json")   // 由于统一返回结果中字符串的特殊原因, 需要手动设置返回类型为json
    public String updateBook(BookInfo bookInfo) {
        log.info("更新图书信息, bookInfo:{}", bookInfo);
        try {
            Integer result = bookService.updateBookById(bookInfo);
            if (result == 0) {
                return "更新失败, 请联系管理员!";
            }
        } catch (Exception e) {
            log.error("更新失败, e:", e);
            return "更新失败, 请联系管理员!";
        }
        return "";
    }

    /**
     * 删除图书信息
     * @param bookId
     * @return
     */
    @RequestMapping("/deleteBook")
    public Result deleteBook(Integer bookId) {
        log.info("删除图书信息, bookId:{}", bookId);
        try {
            BookInfo bookInfo = new BookInfo();
            bookInfo.setId(bookId);
            bookInfo.setStatus(0);
            Integer result = bookService.updateBookById(bookInfo);
            if (result == 0) {
                return Result.error("删除失败, 请联系管理员!");
            }
        } catch (Exception e) {
            log.error("删除失败, e:", e);
            return Result.error("删除失败, 请联系管理员!");
        }
        return Result.success("");
    }

    /**
     * 批量删除图书信息
     * @param ids
     * @return
     */
    @RequestMapping("/batchDeleteBook")
    public Result batchDelete(@RequestParam List<Integer> ids) {   // 默认绑定到数组, 要想绑定到集合，需要使用RequestParam
        log.info("批量删除图书信息, ids:{}", ids);
        Integer result = bookService.batchDeleteBookByIds(ids);
        if (result == 0) {
            return Result.error("删除失败，请联系管理员！");
        }
        return Result.success("");
    }

}
