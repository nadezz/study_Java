package com.java110.booksystem.service;

import com.java110.booksystem.mapper.UserInfoMapper;
import com.java110.booksystem.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * 根据 name 查询用户信息
     * @param userName
     * @return
     */
    public UserInfo getUserInfoByName(String userName) {
        return userInfoMapper.getUserInfoByName(userName);
    }
}
