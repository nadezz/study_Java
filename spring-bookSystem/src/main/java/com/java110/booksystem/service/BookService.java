package com.java110.booksystem.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.java110.booksystem.mapper.BookMapper;
import com.java110.booksystem.model.BookInfo;
import com.java110.booksystem.model.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookMapper bookMapper;

    /**
     * 带有分页查询的获取图书列表
     * @param page
     * @param pageSize
     * @return
     */
    public PageRequest getBookList(Integer page, Integer pageSize) {
        /** 使用 pageHelper 完成分页查询 **/
        //1. 设置分页参数
        PageHelper.startPage(page,pageSize);
        //2. 执行查询  【紧跟着的第一个select方法会被分页，大坑！！！！！必须紧跟分页参数之后】
        List<BookInfo> bookInfos = bookMapper.getBookList();
        Page<BookInfo> p = (Page<BookInfo>) bookInfos;
        for (BookInfo bookInfo : p) {
            if (bookInfo.getStatus() == 2) {
                bookInfo.setStatusCN("不可借阅");
            } else if(bookInfo.getStatus() == 1){
                bookInfo.setStatusCN("可借阅");
            } else {
                bookInfo.setStatusCN("已删除");
            }
        }
        //3. 封装PageRequest对象
        return new PageRequest(p.getResult(),p.getTotal());
    }

    /**
     * 添加图书
     * @param bookInfo
     * @return
     */
    public Integer insertBook(BookInfo bookInfo) {
        return bookMapper.insertBook(bookInfo);
    }

    /**
     * 根据id查询图书信息
     * @param bookId
     * @return
     */
    public BookInfo queryBookById(Integer bookId) {
        BookInfo bookInfo = bookMapper.queryBookById(bookId);
        if (bookInfo.getStatus() == 2) {
            bookInfo.setStatusCN("不可借阅");
        } else if(bookInfo.getStatus() == 1){
            bookInfo.setStatusCN("可借阅");
        } else {
            bookInfo.setStatusCN("已删除");
        }
        return bookInfo;
    }

    /**
     * 更新图书信息
     * @param bookInfo
     * @return
     */
    public Integer updateBookById(BookInfo bookInfo) {
        return bookMapper.updateBookById(bookInfo);
    }


    /**
     * 批量删除图书
     * @param ids
     * @return
     */
    public Integer batchDeleteBookByIds(List<Integer> ids) {
        return bookMapper.batchDeleteBookByIds(ids);
    }
}
