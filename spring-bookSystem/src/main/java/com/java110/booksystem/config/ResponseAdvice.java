package com.java110.booksystem.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java110.booksystem.common.Result;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice   // 统一响应结果
public class ResponseAdvice implements ResponseBodyAdvice {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    @SneakyThrows   // 捕捉异常
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (body instanceof Result) {  // 判断类型
            return body;
        }
        if (body instanceof String) {  // String 类型需要先对内容进行【序列化】再返回,否则报错,通过观察源码知道主要原因是类型不匹配,传入的是Result,实际需要接收的是String类型
            return objectMapper.writeValueAsString(Result.success(body));   // 因此将Result类型通过序列化转换成String类型，即可解决报错
        }
        return Result.success(body);
    }
}
