package com.java110.booksystem.config;

import com.java110.booksystem.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice  // 统一异常处理注解
@Slf4j
public class ExceptionAdvice {

    /**
     * 【全局捕获异常】
     * @param e
     * @return
     */
    @ExceptionHandler
    public Result handlerException(Exception e) {   // 捕获所有异常
        log.error("异常信息: {}",e.getMessage());
        return Result.error(e.getMessage());
    }

    /**
     * 【捕获空指针异常】
     * 源码底层（类名为：ExceptionHandlerMethodResolver）使用一个名为matches的List来存储符合能够捕获该异常的方法,
     * 然后根据Exception的深度进行排序, 再取深度最小的异常方法执行
     * @param e
     * @return
     */
    @ExceptionHandler
    public Result handlerException(NullPointerException e) {   // 捕获空指针异常
        log.error("空指针异常信息: {}",e.getMessage());
        return Result.error(e.getMessage());
    }
}
