package com.java110.booksystem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookInfo {
    private Integer id;
    private String bookName;
    private String author;
    private Integer count;
    private BigDecimal price;
    private String publish;
    private Integer status;  // 0.删除  1.正常  2.不可借阅
    private String StatusCN; // 状态转换字符串，只存在于程序中
    private Date createTime;
    private Date updateTime;
}
