package com.java110.booksystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBookSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBookSystemApplication.class, args);
    }

}
