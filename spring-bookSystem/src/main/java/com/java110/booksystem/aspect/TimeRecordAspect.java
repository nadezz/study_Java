package com.java110.booksystem.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class TimeRecordAspect {
    /**
     * 记录耗时
     * @param joinPoint 连接点（函数）
     * @return 返回目标方法结果
     */
    @Around("execution(* com.java110.booksystem.controller.*.*(..))")  // 所有类的所有方法
    public Object timeRecord (ProceedingJoinPoint joinPoint) throws Throwable {
        // 记录开始时间
        long start = System.currentTimeMillis();
        // 执行目标方法
        Object proceed = joinPoint.proceed();
        // 记录结束时间
        long end = System.currentTimeMillis();
        // 日志打印耗时
        log.info("耗时时间: " + (end - start) + "ms");
        // 返回目标方法结果
        return proceed;
    }
}
