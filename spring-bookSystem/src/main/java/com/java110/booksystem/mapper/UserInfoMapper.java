package com.java110.booksystem.mapper;

import com.java110.booksystem.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserInfoMapper {
    /**
     * 根据 name 查询用户信息
     * @return
     */
    @Select("select * from user_info where user_name = #{name} and delete_flag = 0")
    UserInfo getUserInfoByName(String name);
}
