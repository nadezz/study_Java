package com.java110.booksystem.mapper;

import com.java110.booksystem.model.BookInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface BookMapper {

    /**
     * 获取图书信息列表
     * @return
     */
    @Select("select * from book_info where status != 0")
    List<BookInfo> getBookList();

    /**
     * 新增图书
     * @param bookInfo
     * @return
     */
    @Insert("insert into book_info (book_name, author, count, price, publish, status) " +
            "VALUES (#{bookName},#{author},#{count},#{price},#{publish},#{status})")
    Integer insertBook(BookInfo bookInfo);

    /**
     * 根据id查询图书信息
     * @param bookId
     * @return
     */
    @Select("select * from book_info where id = #{bookId}")
    BookInfo queryBookById(Integer bookId);

    /**
     * 更新图书信息
     * @param bookInfo
     * @return
     */
    Integer updateBookById(BookInfo bookInfo);

    /**
     * 批量删除图书
     * @param ids
     * @return
     */
    Integer batchDeleteBookByIds(List<Integer> ids);
}
