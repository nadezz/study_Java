import book.BookList;
import operation.IOperation;
import user.AdminUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

public class Main {

    public static User login() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的名字：");
        String name = sc.nextLine();
        System.out.println("请输入你的身份， 1：管理员  2：普通用户 ==》");
        int choice = sc.nextInt();
        if(choice == 1) {
            return new AdminUser(name);  //抽象类
        } else {
            return new NormalUser(name);
        }
    }
    public static void main(String[] args) {
        BookList bookList = new BookList();
        //此时user指向的是【管理员】还是【普通用户】是不知道的
        User user = login(); //向上转型
        while (true) {
            int choice = user.menu();   //多态
            //根据选择执行对应的操作
            user.doOperation(choice,bookList);
        }

    }
}
