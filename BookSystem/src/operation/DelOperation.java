package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class DelOperation implements IOperation {
    @Override
    public void work(BookList bookList) {
        Scanner sc = new Scanner(System.in);
        System.out.println("删除图书！");
        System.out.println("请写出你要删除图书的书名：");
        String name = sc.nextLine();
        int currentSize = bookList.getUsedSize();
        int index = -1;
        int i = 0;
        for (; i < currentSize; i++) {
            if (bookList.getBook(i).getName().equals(name)) {
                index = i;
                break;
            }
        }
        if (i > currentSize) {
            System.out.println("不存在书名为'" + name + "'的书籍，无法删除！");
            return;
        }
        //使用移动覆盖作为删除
        for (int j = index; j < currentSize - 1; j++) {
            Book book = bookList.getBook(j + 1);
            bookList.setBook(book, j);
        }
        //方便系统自动回收
        bookList.setBook(null, currentSize - 1);
        bookList.setUsedSize(currentSize - 1);
        System.out.println("成功删除书名为'" + name + "'的书籍！");
    }
}
