package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书！");
        System.out.println("请写出你要查找的图书的书名：");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                System.out.println("存在这本书，信息如下：");
                System.out.println(book);
                return;
            }
        }
        System.out.println("不存在书名为‘"+name+"’的这本书！");
    }
}
