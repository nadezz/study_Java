package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class AddOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner sc = new Scanner(System.in);
        System.out.println("新增图书！");
        System.out.println("请输入你要新增图书的书名：");
        String name = sc.nextLine();
        System.out.println("请输入你要新增图书的作者：");
        String author = sc.nextLine();
        System.out.println("请输入你要新增图书的价格：");
        int price = sc.nextInt();
        sc.nextLine();  //接收回车，sc.nextInt()不会读掉回车，所以需要先接收回车再进行。或者调换一下位置
        System.out.println("请输入你要新增图书的类型：");  //nextInt和nextLine会存在冲突
        String type = sc.nextLine();
        Book book = new Book(name,author,price,type);
        int currentSize = bookList.getUsedSize();

        for (int i = 0; i < currentSize; i++) {
            Book tmp = bookList.getBook(i);
            if(tmp.getName().equals(name)) {
                System.out.println("存在这本书，不能重复添加！");
                return;
            }
        }

        bookList.setBook(book,currentSize);
        bookList.setUsedSize(currentSize+1);
    }
}
