package com.example.springbootdemo.mybatis.mapper;

import com.example.springbootdemo.mybatis.model.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserInfoMapper {

    @Select("select * from userinfo")
    List<UserInfo> getUserInfoAll();

    @Select("select * from userinfo where delete_flag = #{deleteFlag}")
    List<UserInfo> getUserInfoByDeleteFlag(Integer deleteFlag);

    @Select("select * from userinfo where delete_flag = #{deleteFlag} and gender = #{gender}")
    List<UserInfo> getUserInfo2(Integer deleteFlag, Integer gender);

    @Options(useGeneratedKeys = true, keyProperty = "id")   // 将主键 key 赋值到 userInfo 对象中的 id 属性中
    @Insert("insert into userinfo (username, password, age, gender) " +
            "VALUES (#{username},#{password},#{age},#{gender})")
    Integer insertUserInfo(UserInfo userInfo);

    @Delete("delete from userinfo where id = #{id}")
    Integer delete(Integer id);

    //@Update("update userinfo set password = #{password}, age = #{age}, gender = #{gender} where id = #{id}")
    Integer update(UserInfo userInfo);
}
