package com.example.springbootdemo;

import lombok.Data;

@Data
public class MessageInfo {
    private String from;
    private String to;
    private String say;
}
