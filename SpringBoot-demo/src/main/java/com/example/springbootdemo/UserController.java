package com.example.springbootdemo;

import jakarta.servlet.http.HttpSession;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

@RequestMapping("/user")
@RestController
public class UserController {
    @RequestMapping("/login")
    public Boolean login(String userName, String password, HttpSession httpSession) {
        // 非法参数校验
        if (!StringUtils.hasLength(userName) || !StringUtils.hasLength(password)) {
            return false;
        }
        // 判断操作
        if ("admin".equals(userName) && "admin".equals(password)) {
            // 设置 session
            httpSession.setAttribute("userName",userName);
            return true;
        } else {
            return false;
        }
    }

    @RequestMapping("/index")
    public String getUserName(@SessionAttribute("userName") String userName) {
        return userName;
    }
}
