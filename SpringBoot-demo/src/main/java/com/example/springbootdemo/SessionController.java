package com.example.springbootdemo;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RequestMapping("/session")
@RestController
public class SessionController {

    /**
     * 使用传统方法(severLet)获取 cookie
     * @return
     */
    @RequestMapping("/getCookie1")
    public String getCookie1(HttpServletRequest request, HttpServletResponse response) {   // 属于 spring 的内置对象
/*        // 获取参数
        String name = request.getParameter("name");*/
        Cookie[] cookies = request.getCookies();    // 获取所有的 cookie
        if (cookies != null) {
            Arrays.stream(cookies).forEach(ck -> {
                System.out.println(ck.getName() + ":" + ck.getValue());
            });
        }
        return "获取Cookie成功！";
    }

    /**
     * 使用 spring 框架集成的注解方法(@CookieValue)获取 cookie
     * @param Hacynn
     * @return
     */
    @RequestMapping("/getCookie2")
    public String getCookie2(@CookieValue("Hacynn") String Hacynn) {   // 获取其中某一个 cookie
        // 从 cookie 中获取名为：Hacynn 的值
        return "从 cookie 中获取值：" + Hacynn;
    }

    /**
     * 由于 session 存储在服务器，不能伪造，因此创建一个方法设置 session，用于下面方法获取 session
     * @param request
     * @return
     */
    @RequestMapping("/setSession")
    public String setSession(HttpServletRequest request) {
        // 从 cookie 中获取到 sessionId，根据 sessionId 获取 session 对象，【如果没有则创建一个】
        HttpSession session = request.getSession();
        session.setAttribute("name","zhangsan");
        return "设置 session 成功";
    }

    /**
     * 使用传统方法(severLet)获取 session
     * @return
     */
    @RequestMapping("/getSession1")
    public String getSession1(HttpServletRequest request) {
        // 从 cookie 中获取到 sessionId，根据 sessionId 获取 session 对象
        HttpSession session = request.getSession();
        String name = (String) session.getAttribute("name");
        return "从 session 中获取 name：" + name;
    }

    /**
     * 使用 spring 框架提供的方式获取 session
     * @return
     */
    @RequestMapping("/getSession2")
    public String getSession2(HttpSession session) {
        String name = (String) session.getAttribute("name");
        return "从 session 中获取 name：" + name;
    }

    /**
     * 使用 spring 框架提供的方式获取 session   【再优化】
     * @return
     */
    @RequestMapping("/getSession3")
    public String getSession3(@SessionAttribute("name") String name) {
        //String name = (String) session.getAttribute("name");
        return "从 session 中获取 name：" + name;
    }

    /**
     * 使用传统方法获取 header 信息
     * @param request
     * @return
     */
    @RequestMapping("/getHeader1")
    public String getHeader1(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        return "从 header 中获取信息，userAgent：" + userAgent;
    }

    /**
     * 使用 spring 框架获取 header 信息
     * @param userAgent
     * @return
     */
    @RequestMapping("/getHeader2")
    public String getHeader2(@RequestHeader("User-Agent") String userAgent) {
        //String userAgent = request.getHeader("User-Agent");
        return "从 header 中获取信息，userAgent：" + userAgent;
    }
}
