package com.example.springbootdemo.yml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/yml")
@RestController
public class ReadymlController {

    @Value("${server.port}")
    private Integer port;

    @Value("${mykey}")
    private String key;

    @Value("${mykey.key1}")
    private String key1;

    @Autowired  // 由于student对象中设置了@ConfigurationProperties，因此@Autowired会从yml中注入
    private Student student;

    @Autowired
    private DbType dbType;


    @RequestMapping("/readyml1")
    public String readYml1() {
        return "从yml中获取配置文件：" + port;
    }

    @RequestMapping("/readyml2")
    public String readYml2() {
        return "从yml中获取配置文件：" + key;
    }

    @RequestMapping("/readyml3")
    public String readYml3() {
        return "从yml中获取配置文件：" + key1;
    }

    @RequestMapping("/readStudent")
    public String readStudent() {
        return "从yml中获取配置文件中的student：" + student;
    }

    @RequestMapping("/readDbtype")
    public String readDbtype() {
        return "从yml中获取配置文件中的Dbtype：" + dbType;
    }

}
