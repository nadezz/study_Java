package com.example.springbootdemo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RequestMapping("/response")
//@RestController
//@ResponseBody  修改默认，返回数据
//@Controller 默认返回视图，表示该类被 spring 管理
@Controller
public class ResponseController {

    /**
     * 返回视图
     * @return
     */
    @RequestMapping("/index")
    public String index() {
        return "/index.html";
    }

    /**
     * 返回数据
     * @return
     */
    @ResponseBody   // 返回数据
    @RequestMapping("/indexData1")
    public String indexData1() {
        return "返回数据";
    }


    /**
     * 返回 html 代码片段
     * @return
     */
    @ResponseBody   // 返回数据
    @RequestMapping("/indexData2")
    public String indexData2() {
        return "<h1>我是 html 代码片段</h1>";
    }

    /**
     * 返回集合 HashMap
     * @return
     */
    @ResponseBody
    @RequestMapping("/getMap")
    public HashMap<String,String> getMap() {
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("k1","v1");
        hashMap.put("k2","v2");
        hashMap.put("k3","v3");
        hashMap.put("k4","v4");
        return hashMap;   // spring 会自动识别返回内容将 Content-Type 设置成 json
    }

}
