package com.example.springbootdemo.captcha;

public class Constants {
    public final static String CAPTCHA_SESSION_KEY = "captcha_session_key";
    public final static String CAPTCHA_SESSION_DATE = "captcha_session_date";
}
