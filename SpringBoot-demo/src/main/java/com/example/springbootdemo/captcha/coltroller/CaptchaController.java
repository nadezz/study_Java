package com.example.springbootdemo.captcha.coltroller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.example.springbootdemo.captcha.model.CaptchaProperties;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;



@RequestMapping("/captcha")
@RestController
public class CaptchaController {

    @Autowired
    private CaptchaProperties captchaProperties;

    @RequestMapping("/get")
    public void getCaptcha(HttpServletResponse response, HttpSession session) {
        // 定义图形验证码的长和宽
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(captchaProperties.getWidth(), captchaProperties.getHeight());
        // 返回类型
        response.setContentType("image/jpeg");
        // 禁止缓存
        response.setHeader("Progma","No-cache");
        try {
            // 图形验证码写出，可以写出到文件，也可以写出到流
            lineCaptcha.write(response.getOutputStream());
            session.setAttribute(captchaProperties.getSession().getKey(),lineCaptcha.getCode());
            session.setAttribute(captchaProperties.getSession().getDate(),new Date());
            response.getOutputStream().close(); // 关闭
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping("/check")
    public Boolean check(String inputCode, HttpSession session) {
        if (!StringUtils.hasLength(inputCode)) {
            return false;
        }
        Date saveDate = (Date) session.getAttribute(captchaProperties.getSession().getDate());
        // 超时
        if (System.currentTimeMillis() - saveDate.getTime() > 60 * 1000) {
            return false;
        }
        String saveCode = (String) session.getAttribute(captchaProperties.getSession().getKey());
        return inputCode.equalsIgnoreCase(saveCode);
    }
}
