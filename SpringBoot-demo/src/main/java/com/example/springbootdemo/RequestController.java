package com.example.springbootdemo;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/request")
@RestController
public class RequestController {
    @RequestMapping("/hellomvc")  // RequestMapping 建立链接，路由映射
    public String say() {
        return "hello, spring mvc";
    }

    @RequestMapping("/r1")
    public String r1(String name) {
        return "接收到参数，name：" + name;
    }

    @RequestMapping("/r2")
    public String r2(Integer age) {   // 空参传递时，默认赋值 null，包装类可以存储 null
        return "接收到参数，age：" + age;
    }

    @RequestMapping("/r3")
    public String r3(int age) {     // int 不能存储 null ，报错码500
        return "接收到参数，age：" + age;
    }

    @RequestMapping("/r4")
    public String r4(Student student) {
        return "接收到参数，student：" + student;
    }

    @RequestMapping("/r5")
    public String r5(@RequestParam("name") String userName) {  // 默认不能为空
        return "接收到参数，userName：" + userName;
    }

    @RequestMapping("/r6")
    public String r6(@RequestParam(value = "name",required = false) String userName) {   // 修改成可为空
        return "接收到参数，userName：" + userName;
    }

    /**
     * 接收数组
     * @param arr
     * @return
     */
    @RequestMapping("/r7")
    public String r7(int[] arr) {
        return "接收到参数，arr：" + Arrays.toString(arr);
    }

    /**
     * 接收集合
     * @param list
     * @return
     */
    @RequestMapping("/r8")
    public String r8(@RequestParam List<String> list) {  // list=12,14,16 这样传参默认绑定成【数组】，需要使用 @RequestParam 解决该问题
        return "接收到参数，list：" + list;                 // @RequestParam 可以简单理解为“赋值”操作
    }

    /**
     * 接收 json    json 数据存放在 body 中，需要使用 @RequestBody 接收
     * @param student
     * @return
     */
    @RequestMapping("/r9")
    public String r9(@RequestBody Student student) { // 使用 @RequestBody 从请求头的 body 中获取 json
        return "接收到参数，student：" + student;
    }

    /**
     * 从路径中获取参数
     * @return
     */
    @RequestMapping("/r10/{articleId}/{name}")
    public String r10(@PathVariable("articleId") Integer articleId, @PathVariable("name") String name) {
        return "接收到参数，articleId：" + articleId + ", name：" + name;
    }

    /**
     * 上传文件
     * @return
     */
    @RequestMapping("/r11")
    public String r11(@RequestPart("file") MultipartFile file) {  //  @RequestPart 与 @RequestParam 比较类似，都是重命名
        String originalFilename = file.getOriginalFilename();
        return "接收到文件，文件名称：" +  originalFilename;
    }
}
