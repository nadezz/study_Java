package com.example.springbootdemo.facade.light;

public class HallLight implements Light{
    @Override
    public void on() {
        System.out.println("开启走廊灯");
    }

    @Override
    public void off() {
        System.out.println("关闭走廊灯");
    }
}
