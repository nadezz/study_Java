package com.example.springbootdemo.facade.light;

public interface Light {
    void on();
    void off();
}
