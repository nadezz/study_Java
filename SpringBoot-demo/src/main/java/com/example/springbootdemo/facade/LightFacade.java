package com.example.springbootdemo.facade;

import com.example.springbootdemo.facade.light.BedRoomLight;
import com.example.springbootdemo.facade.light.HallLight;
import com.example.springbootdemo.facade.light.Light;

public class LightFacade {

    private Light bedRoomLight = new BedRoomLight();
    private Light hallLight = new HallLight();
    public void lightOn() {
        bedRoomLight.on();
        hallLight.on();
    }

    public void lightOff() {
        bedRoomLight.off();
        hallLight.off();
    }
}
