package com.example.springbootdemo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONTest {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonStr = "{\"name\":\"zhangsan\",\"id\":1,\"age\":15}";
        // 字符串转对象
        Student student = objectMapper.readValue(jsonStr, Student.class);
        System.out.println(student);
        // 对象转字符串
        String s = objectMapper.writeValueAsString(student);
        System.out.println(s);
    }
}
