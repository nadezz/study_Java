package com.example.springbootdemo.mybatis.mapper;

import com.example.springbootdemo.mybatis.model.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class UserInfoMapperTest {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Test
    void getUserInfoAll() {
        List<UserInfo> userInfos = userInfoMapper.getUserInfoAll();
        System.out.println(userInfos);
    }

    /**
     * 参数传递
     */
    @Test
    void getUserInfoByDeleteFlag() {
        List<UserInfo> userInfo = userInfoMapper.getUserInfoByDeleteFlag(1);
        System.out.println(userInfo);
    }

    @Test
    void getUserInfo2() {
        System.out.println(userInfoMapper.getUserInfo2(0, 1));
    }

    @Test
    void insertUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("zhaoliu");
        userInfo.setPassword("zhaoliu");
        userInfo.setAge(12);
        userInfo.setGender(1);
        int ret = userInfoMapper.insertUserInfo(userInfo);
        System.out.println("改变行数："+ ret +", 主键id返回：" + userInfo.getId());
    }

    @Test
    void delete() {
        System.out.println("删除数据："+userInfoMapper.delete(6));
    }

    @Test
    void update() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(2);
        userInfo.setPassword("123456666");
        userInfo.setAge(20);
        userInfo.setGender(2);
        userInfoMapper.update(userInfo);
    }

}