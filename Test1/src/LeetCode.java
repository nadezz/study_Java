import java.util.*;

import static java.util.Arrays.copyOf;

public class LeetCode {


    /**
     * 递归找最大值
     */
    public static int process(int[] arr,int left,int right) {
        if(left == right) {
            return arr[left];
        }
        int mid = left + (right - left) / 2;
        int leftMax = process(arr,left,mid);
        int rightMax = process(arr,mid+1,right);
        return Math.max(leftMax, rightMax);
    }

    public static void MergeProcess(int[] arr, int left, int right) {
        if(left == right) {
            return;
        }
        int mid = left + (right - left) / 2;
        MergeProcess(arr,left,mid);
        MergeProcess(arr,mid + 1,right);
        MergeSort(arr,left,mid,right);
    }

    public static void MergeSort(int[] arr, int left, int mid, int right) {
        int[] helpArr = new int[right - left + 1];
        int p1 = left;
        int p2 = mid + 1;
        int i = 0;
        while(p1 <= mid && p2 <= right) {
            helpArr[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        while(p1 <= mid) {
            helpArr[i++] = arr[p1++];
        }
        while(p2 <= right) {
            helpArr[i++] = arr[p2++];
        }
        for(i = 0 ; i < helpArr.length; i++) {
            arr[left + i] = helpArr[i];
        }
    }

    public static void main(String[] args) {
//        int[] arr = {5,4,2,7,95,1,8,23};
//        int ret = process(arr,0,arr.length-1);
//        System.out.println(ret);
        int[] arr = {5,4,2,7,95,1,8,23};
        MergeProcess(arr,0,arr.length-1);
        for(int x : arr) {
            System.out.print(x+" ");
        }
        System.out.println();
    }

//    public static void main(String[] args) {
//        int[] nums = {1,3,-1,-3,5,3,6,7};
//        int k = 3;
//        System.out.println(Arrays.toString(maxSlidingWindow(nums, k)));
//    }
//    public static int[] maxSlidingWindow(int[] nums, int k) {
//        int len = nums.length - k + 1;
//        int[] compare = copyOf(nums,len);
//        int[] ret = new int[len];
//
//        PriorityQueue<Integer> pq = new PriorityQueue<>(new Comparator() {
//            @Override
//            public int compare(Object o1, Object o2) {
//                return (int)o2-(int)o1;
//            }
//
//        });
//        for(int i = 0; i < k; i++) {
//            pq.add(nums[i]);
//        }
//        for(int i = 0; i < len; i++) {
//            ret[i] = pq.peek();
//            pq.remove(compare[i]);
//            pq.add(nums[i+k]);
//        }
//        return ret;
//    }


//    public static void main(String[] args) {
//        String s = "baa";
//        String p = "aa";
//        System.out.println(findAnagrams(s,p));
//    }
//    public static List<Integer> findAnagrams(String s, String p) {
//        HashSet<Character> hash = new HashSet<>();
//        List<Integer> ret = new ArrayList<>();
//        int len = p.length();
//        for(int i = 0; i < s.length() - len + 1; i++) {
//            for(int j = i; j < i + len; j++) {
//                if( !hash.add(s.charAt(j)) ) {
//                    break;
//                }
//            }
//            boolean flag = true;
//            for(int k = 0;k < len; k++) {
//                if(hash.add(p.charAt(k))) {
//                    flag = false;
//                }
//            }
//            if(flag) {
//                ret.add(i);
//            }
//            hash.clear();
//        }
//        return ret;
//    }


//    public static void main(String[] args) {
//        String s = "pwwkew";
//        int ret = lengthOfLongestSubstring(s);
//        System.out.println(ret);
//    }
//
//
//    public static int lengthOfLongestSubstring(String s) {
//        int max = 0;
//        HashSet<Character> hash = new HashSet<>();
//        for(int i = 0; i < s.length(); i++) {
//            int count = 0;
//            for(int j = i; j < s.length(); j++) {
//                if( !hash.add( s.charAt(j) ) ) {
//                    hash.clear();
//                    break;
//                }
//                count++;
//            }
//            max = Math.max(max,count);
//        }
//        return max;
//
//    }

//    public static void main(String[] args) {
//        int[] height = {0,1,0,2,1,0,1,3,2,1,2,1};
//        int ret = trap(height);
//        System.out.println(ret);
//
//    }
//    public static int trap(int[] height) {
//        int left = 0;
//        int right = height.length - 1;
//        int sum = 0;
//        int max = heightMax(height);
//        for(int i = 1; i <= max; i++) {
//            if(left < right) {
//                while(height[left] < i) {
//                    left++;
//                }
//                while(height[right] < i) {
//                    right--;
//                }
//                int ret = layerNumber(height,left,right,i);
//                sum += ret;
//            }
//
//        }
//        return sum;
//    }
//
//    public static int heightMax(int[] arr) {
//        int max = arr[0];
//        for(int i = 1; i < arr.length; i++) {
//            max = Math.max(max, arr[i]);
//        }
//        return max;
//    }
//
//    public static int layerNumber(int[] arr, int left, int right, int signal) {   //signal是当前所在曾
//        int len = right - left + 1;
//        for(int i = left; i <= right; i++) {
//            if(arr[i] >= signal) {
//                len--;
//            }
//        }
//        return len;
//    }
}

