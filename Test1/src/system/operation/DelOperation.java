package system.operation;

import system.book.Book;
import system.book.BookList;

import java.util.Scanner;

public class DelOperation implements IOperation {
    @Override
    public void work(BookList bookList) {
        System.out.println("删除图书！");
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入要删除的书名：");
        String name = sc.nextLine();

        int size = bookList.getUsedSize();
        int i = 0;
        for (; i < size; i++) {
            if (bookList.getBook(i).getName().equals(name)) {
                break;
            }
        }
        if (i < size) {
            for (int j = i; j < size - 1; j++) {
                Book tmp = bookList.getBook(j + 1);
                bookList.setBook(tmp, j);
            }
            bookList.setUsedSize(size - 1);
            System.out.println("删除成功！");
        } else {
            System.out.println("不存在书名为'" + name + "'的书籍，删除失败！");

        }
    }
}
