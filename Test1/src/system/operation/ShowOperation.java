package system.operation;

import system.book.Book;
import system.book.BookList;

public class ShowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("显示图书！");
        int size = bookList.getUsedSize();
        for (int i = 0; i < size; i++) {
            Book tmp = bookList.getBook(i);
            System.out.println(tmp);
        }
    }
}
