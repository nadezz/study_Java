package system.operation;

import system.book.BookList;

import java.util.Scanner;

public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("归还图书！");
        Scanner sc = new Scanner(System.in);
        System.out.println("写出要归还的书名：");
        String name = sc.nextLine();

        int size = bookList.getUsedSize();
        for (int i = 0; i < size; i++) {
            if (bookList.getBook(i).getName().equals(name)) {
                System.out.println("归还成功！");
                bookList.getBook(i).setBorrow(false);
                return;
            }
        }
        System.out.println("不存在书名为'" + name + "'的书籍，归还失败！");
    }
}
