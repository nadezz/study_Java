package system.operation;

import system.book.Book;
import system.book.BookList;

import java.util.Scanner;

public class AddOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("添加图书！");
        Scanner sc = new Scanner(System.in);
        System.out.println("输入书名：");
        String name = sc.nextLine();
        System.out.println("输入作者：");
        String author = sc.nextLine();
        System.out.println("输入价格：");
        int price = sc.nextInt();
        sc.nextLine(); //吃掉回车
        System.out.println("输入类型：");
        String type = sc.nextLine();

        int size = bookList.getUsedSize();
        for (int i = 0; i < size; i++) {
            Book tmp = bookList.getBook(i);
            if(tmp.getName().equals(name)) {
                System.out.println("该书已存在，无法添加！");
                return;
            }
        }
        Book book = new Book(name,author,price,type);
        bookList.setBook(book,size);
        bookList.setUsedSize(size+1);
    }
}
