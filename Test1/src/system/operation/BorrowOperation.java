package system.operation;

import system.book.BookList;

import java.util.Scanner;

public class BorrowOperation implements IOperation {
    @Override
    public void work(BookList bookList) {
        System.out.println("借阅图书！");
        Scanner sc = new Scanner(System.in);
        System.out.println("写出要借阅的书名：");
        String name = sc.nextLine();

        int size = bookList.getUsedSize();
        for (int i = 0; i < size; i++) {
            if (bookList.getBook(i).getName().equals(name)) {
                System.out.println("借阅成功！");
                bookList.getBook(i).setBorrow(true);
                return;
            }
        }
        System.out.println("不存在书名为'" + name + "'的书籍，借阅失败！");
    }
}
