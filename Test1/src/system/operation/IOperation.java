package system.operation;

import system.book.BookList;

public interface IOperation {
    public abstract void work(BookList bookList);
}
