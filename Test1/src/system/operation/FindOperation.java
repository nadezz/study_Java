package system.operation;

import system.book.BookList;

import java.util.Scanner;

public class FindOperation implements IOperation {
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书！");
        Scanner sc = new Scanner(System.in);
        System.out.println("输入要查找的书名：");
        String name = sc.nextLine();

        int size = bookList.getUsedSize();
        for (int i = 0; i < size; i++) {
            if (bookList.getBook(i).getName().equals(name)) {
                System.out.println(bookList.getBook(i));
                return;
            }
        }
        System.out.println("不存在书名为'" + name + "'的书籍，查找失败！");
    }
}
