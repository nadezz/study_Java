package system.user;

import system.book.BookList;
import system.operation.IOperation;

public abstract class User {
    protected String name;
    protected IOperation[] iOperations;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public abstract int menu();

    public void Operation(int choice, BookList bookList) {
        this.iOperations[choice].work(bookList);
    }
}
