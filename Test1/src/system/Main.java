package system;

import system.book.BookList;
import system.user.AdminUser;
import system.user.NormalUser;
import system.user.User;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BookList bookList = new BookList();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入姓名：");
        String name = sc.nextLine();
        System.out.println("请选择用户身份： 1.管理员  2.普通用户");
        int choice = sc.nextInt();
        User user;
        if (choice == 1) {
            user = new AdminUser(name);
        } else {
            user = new NormalUser(name);
        }

        while (true) {
            int index = user.menu();
            user.Operation(index,bookList);
        }
    }
}
