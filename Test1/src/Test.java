import java.lang.reflect.Array;
import java.util.*;




public class Test {
    public static void main(String[] args) {
        Scanner scc = new Scanner(System.in);
    }

    //存在连续三个奇数的数组
    public static boolean threeOdd2(int[] arr) {
        for (int i = 0; i < arr.length - 2; i++) {
            if(arr[i]%2==1 && arr[i+1]%2==1 && arr[i+2]%2==1) {
                return true;
            }
        }
        return false;
    }
    public static void main39(String[] args) {
        int[] arr = {1,2,34,3,4,5,7,23,12};
        System.out.println(threeOdd2(arr));
    }

    public static int[] threeOdd1(int[] arr) {
        for (int i = 0; i < arr.length - 2; i++) {
            if(arr[i]%2==1 && arr[i+1]%2==1 && arr[i+2]%2==1) {
                return new int[] {arr[i],arr[i+1],arr[i+2]};
            }
        }
        return new int[0];
    }
    public static void main38(String[] args) {
        int[] arr = {1,2,34,3,4,5,7,23,12};
        System.out.println(Arrays.toString(threeOdd1(arr)));

    }

    //多数元素
    public static int mostElement(int[] arr) {
        int n = arr.length / 2;
        int[] num = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            num[arr[i]]++;
        }
        for (int i = 0; i < num.length; i++) {
            if(n < num[i]) {
                return i;
            }
        }
        return -1;
    }
    public static void main37(String[] args) {
        int[] arr = {2,2,1,1,1,2,2};
        System.out.println(mostElement(arr));
    }

    //只出现一次的数字
    public static int appearOne(int[] arr) {
        int ret = 0;
        for (int i = 0; i < arr.length; i++) {
            ret ^= arr[i];
        }
        return ret;
    }
    public static void main36(String[] args) {
        int[] arr = {4,1,2,1,2};
        System.out.println(appearOne(arr));
    }

    //两数之和 LeetCode原题
    public static int[] sumOfTwo(int[] arr, int k) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i; j < arr.length; j++) {
                if(arr[i] + arr[j] == k) {
                    return new int[] {i,j};
                }
            }
        }
        return new int[0];
    }
    public static void main35(String[] args) {
        int[] arr = {2,7,11,15};
        System.out.println(Arrays.toString(sumOfTwo(arr, 9)));
    }

    //奇数位于偶数之前
    public static void adjust(int[] arr) {
        int left = 0;
        int right = arr.length - 1;
        while (left < right) {
            while (arr[left] % 2 == 1) {
                left++;
            }
            while (arr[right] % 2 == 0) {
                right--;;
            }
            if (left < right) {
                int tmp = arr[left];
                arr[left] = arr[right];
                arr[right] = tmp;
            }
        }
    }
    public static void main34(String[] args) {
        int[] arr = {1,2,3,4,5,6};
        adjust(arr);
        System.out.println(Arrays.toString(arr));
    }
    //数组所有元素之和
    public static int sum(int[] arr)  {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
    public static void main33(String[] args) {
        int[] arr = {1,2,3,4};
        System.out.println(sum(arr));
    }

    //改变原有数组元素的值，元素扩大为2倍
    public static void transform(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] * 2;
        }
    }
    public static void main32(String[] args) {
        int[] arr = {1,2,3,4};
        transform(arr);
        System.out.println(Arrays.toString(arr));
    }

    //创建的数组，并且赋初始值
    public static void main31(String[] args) {
        int[] arr = new int[100];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i+1;
        }
        System.out.println(Arrays.toString(arr));
    }

    //冒泡排序
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            boolean flag = false;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if(arr[j] > arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                    flag = true;
                }
            }
            if(flag == false) {
                return;
            }
        }
    }
    public static void main30(String[] args) {
        int[] arr = {21, 24, 12, 3, 7, 35, 66};
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }



    //
    public static int binarySearch(int[] arr, int k) {
        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int mid = (left + right) / 2;
            if (arr[mid] < k) {
                left = mid + 1;
            } else if (arr[mid] > k) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    public static void main29(String[] args) {
        int[] arr = {21, 24, 12, 3, 7, 35, 66};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(binarySearch(arr, 35));
    }


    //复制特定范围的数组
    public static void main28(String[] args) {
        int[] arr = {1, 2, 3, 4};
        int[] copy = Arrays.copyOfRange(arr, 1, 3);  //[1,3)左闭右开
        System.out.println(Arrays.toString(copy));
    }

    //使用复制数组方法扩大两倍的数组空间
    public static void main27(String[] args) {
        int[] arr = {1, 2, 3, 4};
        arr = Arrays.copyOf(arr, arr.length * 2);
        System.out.println(Arrays.toString(arr));
    }

    //将整型数组转化为字符串
    public static String my_toString(int[] arr) {
        if (arr == null) {
            return "null";
        }
        String str = "[";
        for (int i = 0; i < arr.length; i++) {
            str += arr[i];
            if (i != arr.length - 1) {
                str += ",";
            }
        }
        str += "]";
        return str;
    }

    public static void main26(String[] args) {
        int[] arr = {1, 2, 3, 4};
        System.out.println(my_toString(arr));
    }


    //递归求解汉诺塔问题
    public static void move(char pos1, char pos2) {
        System.out.print(pos1 + "->" + pos2 + " ");
    }

    public static void Hanoi(int n, char pos1, char pos2, char pos3) {
        if (n == 1) {
            move(pos1, pos3);
            return;
        }
        Hanoi(n - 1, pos1, pos3, pos2);
        move(pos1, pos3);
        Hanoi(n - 1, pos2, pos1, pos3);
    }

    public static void main25(String[] args) {
        Hanoi(3, 'a', 'b', 'c');
    }

    //递归求斐波那契数列的第 N 项
    public static int Fib(int n) {
        if (n <= 2) {
            return 1;
        }
        return Fib(n - 1) + Fib(n - 2);
    }

    public static void main24(String[] args) {
        System.out.println(Fib(11));
    }

    //写一个递归方法，输入一个非负整数，返回组成它的数字之和
    public static int func4(int n) {
        if (n < 10) {
            return n;
        }
        return func4(n / 10) + n % 10;
    }

    public static void main23(String[] args) {
        System.out.println(func4(1234));
    }

    //递归打印数字的每一位
    public static void func3(int n) {
        if (n < 10) {
            System.out.print(n + " ");
            return;
        }
        func3(n / 10);
        System.out.print(n % 10 + " ");
    }

    public static void main22(String[] args) {
        func3(1234);
    }

    //递归求和
    public static int func2(int n) {
        if (n == 1) {
            return 1;
        }
        return n + func2(n - 1);
    }

    public static void main21(String[] args) {
        System.out.println(func2(10));
    }

    //递归求 N 的阶乘
    public static int func1(int n) {
        if (n == 1) {
            return 1;
        }
        return n * func1(n - 1);
    }

    public static void main20(String[] args) {
        System.out.println(func1(5));
    }

    public static int max(int a, int b) {
        return a > b ? a : b;
    }

    public static double max(double a, double b, double c) {
        double ret = a > b ? a : b;
        return ret > c ? ret : c;
    }

    //求最大值方法的重载
    public static void main19(String[] args) {
        int a = 2, b = 5;
        double c = 3.2, d = 4.6, e = 7;
        System.out.println(max(a, b));
        System.out.println(max(c, d, e));
    }


    //求和的重载
    public static int add(int a, int b) {
        return a + b;
    }

    public static double add(double a, double b, double c) {
        return a + b + c;
    }

    public static void main18(String[] args) {
        int a = 2, b = 5;
        double c = 3.2, d = 4.6, e = 7;
        System.out.println(add(a, b));
        System.out.println(add(c, d, e));
    }


    //斐波那契数列
    public static void main17(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 1;
        int b = 1;
        int c = 0;
        int n = sc.nextInt();
        if (n <= 2) {
            c = 1;
        } else {
            while ((n - 2) != 0) {
                c = a + b;
                a = b;
                b = c;
                n--;
            }
        }
        System.out.println(c);
    }

    //使用函数求最大值
    public static int max2(int a, int b) {
        return a > b ? a : b;
    }

    public static int max3(int a, int b, int c) {
        int ret = max2(a, b);
        return ret > c ? ret : c;
    }

    public static void main16(String[] args) {
        int a = 20;
        int b = 35;
        int c = 15;
        System.out.println(max3(a, b, c));
    }


    //模拟登陆
    public static void main15(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 3;
        String pwd = "abc123";
        for (int i = 0; i < n; i++) {
            System.out.println("请输入密码，最多输入三次，当前还剩余" + (n - i) + "次");
            String ret = sc.nextLine();
            if (pwd.equals(ret)) {
                System.out.println("登录成功！");
                break;
            }
        }
    }

    //输出一个整数的每一位
    public static void main14(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个整数：");
        int num = sc.nextInt();
        while (num != 0) {
            System.out.print(num % 10 + " ");
            num /= 10;
        }
    }


    //计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值
    public static void main13(String[] args) {
        double sum = 0;
        int flag = 1;
        for (int i = 1; i <= 100; i++) {
            sum += (1.0 / i) * flag;
            flag = -flag;
        }
        System.out.println(sum);
    }


    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
            return true;
        }
        return false;
    }

    public static void main12(String[] args) {
        System.out.println(isLeapYear(2004));
    }


    //计算二进制中1的个数
    public static void main11(String[] args) {
        int n = 15;
        int count = 0;
        while (n != 0) {
            count++;
            n = n & (n - 1);   //每次消掉一个1
        }
        System.out.println(count);
    }

    //打印0~99999的水仙花数
    public static void main10(String[] args) {
        for (int i = 1; i <= 99999; i++) {
            int tmp = i;
            int count = 0;
            while (tmp != 0) {
                count++;
                tmp /= 10;
            }
            int sum = 0;
            tmp = i;
            while (tmp != 0) {
                sum += (int) Math.pow(tmp % 10, count);
                tmp /= 10;
            }
            if (sum == i) {
                System.out.println(i);
            }
        }
    }


    //最大公约数
    public static void main9(String[] args) {
        int a = 24;
        int b = 18;
        int c = a % b;
        while (c != 0) {
            a = b;
            b = c;
            c = a % b;
        }
        System.out.println(b);
    }


    //乘法口诀表
    public static void main8(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "*" + i + "=" + (i * j) + "  ");
            }
            System.out.println();
        }
    }

    public static void main7(String[] args) {
        Scanner sc = new Scanner(System.in);

        Random r = new Random();
        int rand = r.nextInt(100) + 1;

        while (true) {
            System.out.println("请输入要猜的数：");
            int num = sc.nextInt();
            if (num < rand) {
                System.out.println("猜小了！");
            } else if (num > rand) {
                System.out.println("猜大了！");
            } else {
                System.out.println("猜对了！");
                break;
            }
        }
    }

    public static void main6(String[] args) {
        int sum = 0;
        int ret = 1;
        for (int n = 1; n <= 5; n++) {
            ret *= n;
            sum += ret;
        }
        System.out.println(sum);

    }

    public static void main5(String[] args) {
        for (int i = 0; i <= 100; i++) {
            if (i % 3 != 0 || i % 5 != 0) {
                continue;
            }
            System.out.print(i + " ");
        }
    }

    //1-100中9出现的次数
    public static void main4(String[] args) {
        int count = 0;
        for (int i = 1; i <= 100; i++) {
            if (i % 10 == 9) {
                count++;
            }
            if (i / 10 == 9) {
                count++;
            }
        }
        System.out.println(count);
    }

    //闰年
    public static void main3(String[] args) {
        for (int i = 1000; i <= 2000; i++) {
            if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0)) {
                System.out.print(i + " ");
            }
        }
    }

    //判断素数
    public static void main2(String[] args) {
        for (int n = 1; n < 100; n++) {
            int i = 0;
            for (i = 2; i <= n; i++) {
                if (n % i == 0) {
                    break;
                }
            }
            if (i == n) {
                System.out.print(n + " ");
            }
        }
    }

    public static void main1(String[] args) {
        int n = 5;
        int ret = 1;
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            ret *= i;
            sum += ret;
        }
        System.out.println(sum);
    }
}
